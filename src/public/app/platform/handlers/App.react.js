var React = require('react');



// Get components
var AppView = require('../layout/App/components/App.react');



// Set component : AppHandler
var AppHandler = React.createClass({

	// Render
	render: function() {
		return (
			<AppView>{this.props.children}</AppView>
		);
	}

});


// Exports component
module.exports = AppHandler;
