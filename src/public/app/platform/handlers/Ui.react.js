var React = require('react');



// Get components
var UiView = require('../views/Ui/components/Ui.react');



// Set component : UiHandler
var UiHandler = React.createClass({
	render: function() {
		return (
			<UiView />
		);
	}
});



// Exports component
module.exports = UiHandler;