var React = require('react');
var Router = require('react-router');
var Moment = require('moment');


// Set component : Ui
var Ui = React.createClass({

	// Get initial state
	getInitialState: function() {
		return {
			photoInputProgress: 0
		};
	},

	// After component mount
	componentDidMount: function() {

		/*

		// Progress
		setInterval(function() {

			// Data
			var photoInputProgress = this.state.photoInputProgress;

			// If progress reached 100
			if(photoInputProgress !== 0 && photoInputProgress%100 === 0) {
				photoInputProgress = 0;
			} else {
				photoInputProgress++;
			}

			// Set state
			this.setState({
				photoInputProgress: photoInputProgress
			});

		}.bind(this), 20);

		*/

	},

	// Render
	render: function() {
		return (
			<div>

				{/* BUTTONS */}

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="white"><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" disabled><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" loading><span>Annuler</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="white" dropdown><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" dropdown disabled><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" dropdown loading><span>Annuler</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="white" small><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" small disabled><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" small loading><span>Annuler</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="white" dropdown small><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" dropdown small disabled><span>Annuler</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="white" dropdown small loading><span>Annuler</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="blue"><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="blue" dropdown><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" dropdown disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" dropdown loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="blue" small><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" small disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" small loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="blue" dropdown small><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" dropdown small disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="blue" dropdown small loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="link"><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="link" dropdown><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" dropdown disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" dropdown loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="link" small><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" small disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" small loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.Button type="link" dropdown small><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" dropdown small disabled><span>Enregistrer</span></Nextrap.Button>
					</li>
					<li>
						<Nextrap.Button type="link" dropdown small loading><span>Enregistrer</span></Nextrap.Button>
					</li>
				</ul>

				{/* /BUTTONS */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* BUTTON GROUP */}

				<Nextrap.ButtonGroup>
					<Nextrap.Button type="white"><span>Annuler</span></Nextrap.Button>
					<Nextrap.Button type="blue"><span>Enregistrer</span></Nextrap.Button>
					<Nextrap.Button type="link" dropdown><span>Plus</span></Nextrap.Button>
				</Nextrap.ButtonGroup>

				{/* /BUTTON GROUP */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* BUTTON LIST */}

				<div style={{ width: '200px', background: '#FFF', border: '1px solid #DDD', borderRadius: '2px' }}>
					<Nextrap.ButtonList>
						<Nextrap.ButtonListItem><span>All</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem loading><span>Repertory</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem><span>Projects</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem active><span>Opportunity</span></Nextrap.ButtonListItem>
					</Nextrap.ButtonList>
				</div>

				<br/>

				<div style={{ width: '200px', background: '#FFF', border: '1px solid #DDD', borderRadius: '2px' }}>
					<Nextrap.ButtonList separatedByBorder>
						<Nextrap.ButtonListItem><span>All</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem loading><span>Repertory</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem><span>Projects</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem active><span>Opportunity</span></Nextrap.ButtonListItem>
					</Nextrap.ButtonList>
				</div>

				<br/>

				<div style={{ width: '200px', background: '#FFF', border: '1px solid #DDD', borderRadius: '2px' }}>
					<Nextrap.ButtonList small>
						<Nextrap.ButtonListItem><span>All</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem loading><span>Repertory</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem><span>Projects</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem active><span>Opportunity</span></Nextrap.ButtonListItem>
					</Nextrap.ButtonList>
				</div>

				<br/>

				<div style={{ width: '200px', background: '#FFF', border: '1px solid #DDD', borderRadius: '2px' }}>
					<Nextrap.ButtonList small separatedByBorder>
						<Nextrap.ButtonListItem><span>All</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem loading><span>Repertory</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem><span>Projects</span></Nextrap.ButtonListItem>
						<Nextrap.ButtonListItem active><span>Opportunity</span></Nextrap.ButtonListItem>
					</Nextrap.ButtonList>
				</div>

				{/* /BUTTON LIST */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* VERTICAL NAV GROUP */}

				<div style={{ width: '200px', border: '1px solid #DDD', borderRadius: '2px' }}>
					<Nextrap.VNavList>
						<Nextrap.VNavItem active><span>All</span></Nextrap.VNavItem>
						<Nextrap.VNavItem><span>Repertory</span></Nextrap.VNavItem>
						<Nextrap.VNavItem loading><span>Projects</span></Nextrap.VNavItem>
						<Nextrap.VNavItem><span>Opportunity</span></Nextrap.VNavItem>
					</Nextrap.VNavList>
				</div>

				{/* /VERTICAL NAV GROUP */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* HORIZONTAL NAV GROUP */}

				<div style={{ background: '#FFF' }}>
					<Nextrap.HNavList>
						<Nextrap.HNavItem href="#" active><span>All</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Repertory</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Projects</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Opportunity</span></Nextrap.HNavItem>
					</Nextrap.HNavList>
				</div>

				<br/>

				<div style={{ background: '#FFF' }}>
					<Nextrap.HNavList withoutBorder>
						<Nextrap.HNavItem href="#" active><span>All</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Repertory</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Projects</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Opportunity</span></Nextrap.HNavItem>
					</Nextrap.HNavList>
				</div>

				<br/>

				<div style={{ background: '#F7F7F7' }}>
					<Nextrap.HNavList dark>
						<Nextrap.HNavItem href="#" active><span>All</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Repertory</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Projects</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Opportunity</span></Nextrap.HNavItem>
					</Nextrap.HNavList>
				</div>

				<br/>

				<div style={{ background: '#F7F7F7' }}>
					<Nextrap.HNavList dark withoutBorder>
						<Nextrap.HNavItem href="#" active><span>All</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Repertory</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Projects</span></Nextrap.HNavItem>
						<Nextrap.HNavItem href="#"><span>Opportunity</span></Nextrap.HNavItem>
					</Nextrap.HNavList>
				</div>

				{/* /HORIZONTAL NAV GROUP */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* TEXT */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="text" placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="text" placeholder="Placeholder" initialValue="Filled" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="text" placeholder="Disabled" initialValue="Filled" readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="text" placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="text" placeholder="Placeholder" initialValue="Filled" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="text" placeholder="Disabled" initialValue="Filled" readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="textarea" placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="textarea" placeholder="Placeholder" initialValue="Filled" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText type="textarea" placeholder="Disabled" initialValue="Filled" readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="textarea" placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="textarea" placeholder="Placeholder" initialValue="Filled" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlText transparent type="textarea" placeholder="Disabled" initialValue="Filled" readOnly />
					</li>
				</ul>

				{/* /TEXT */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* STATIC */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlStatic value="Filled" />
					</li>
				</ul>

				{/* /STATIC */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* SELECT */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							disabled />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							loading />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null}
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							disabled />
					</li>
					<li>
						<Nextrap.ControlSelect
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							loading />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							disabled />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							loading />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null}
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							disabled />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="blue"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							loading />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							disabled />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							loading />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue={null}
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							disabled />
					</li>
					<li>
						<Nextrap.ControlSelect
							type="link"
							options={[
								{ label: 'Placeholder', value: null },
								{ label: 'Option 1', value: 'option1' },
								{ label: 'Option 2', value: 'option2' }
							]}
							placeholder={null}
							initialValue="option1"
							compact
							loading />
					</li>
				</ul>

				{/* /SELECT */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* DATE */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")} />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							compact />
					</li>
					<li>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							compact />
					</li>
					<li>
						<Nextrap.ControlDate
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							readOnly
							compact />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							compact />
					</li>
					<li>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							compact />
					</li>
					<li>
						<Nextrap.ControlDate
							type="link"
							format="DD MMM YYYY"
							placeholder={new Date()}
							initialValue={new Date()}
							currentDate={new Date()}
							minDate={Moment().subtract(1, "month")}
							maxDate={Moment().add(1, "month")}
							readOnly
							compact />
					</li>
				</ul>

				{/* /DATE */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* SELECT_BTN */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelectButtons options={[ { label: 'Option 1', value: 'option1' }, { label: 'Option 2', value: 'option2' }, { label: 'Option 3', value: 'option3'} ]} initialValue="option1" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlSelectButtons options={[ { label: 'Option 1', value: 'option1' }, { label: 'Option 2', value: 'option2' }, { label: 'Option 3', value: 'option3'} ]} initialValue="option1" readOnly />
					</li>
				</ul>

				{/* /SELECT_BTN */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* PHOTO */}

				<ul className="h-list marged-list">
					<li style={{ width: '310px' }}>
						<Nextrap.ControlPhoto
  							action="Ajouter une photo"
  							details="Le fichier ne doit pas dépassé 2Mo, les types de fichier supportés sont : jpg, png, gif."
  							placeholderImage={null}
  							initialValue={null}
  							isLoading={false}
  							disabled={false}
          					fileInputAction="Uploader un fichier"
          					fileInputDetails="Ajouter un fichier depuis votre ordinateur"
          					urlInputAction="Fournir une url"
          					urlInputDetails="Ajouter un fichier depuis le web"
          					initialUrlInputValue={""}
          					urlInputPlaceholder="http://monsite.com/ma_photo.jpg" />
					</li>
					<li style={{ width: '310px' }}>
						<Nextrap.ControlPhoto
  							action="Ajout en cours..."
  							details="Le fichier ne doit pas dépassé 2Mo, les types de fichier supportés sont : jpg, png, gif."
  							placeholderImage={null}
  							initialValue={null}
  							isLoading={true}
  							loadingProgress={this.state.photoInputProgress/100}
  							disabled={false}
          					fileInputAction="Uploader un fichier"
          					fileInputDetails="Ajouter un fichier depuis votre ordinateur"
          					urlInputAction="Fournir une url"
          					urlInputDetails="Ajouter un fichier depuis le web"
          					initialUrlInputValue={""}
          					urlInputPlaceholder="http://monsite.com/ma_photo.jpg" />
					</li>
					<li style={{ width: '310px' }}>
						<Nextrap.ControlPhoto
  							action="Modifier la photo"
  							details="Le fichier ne doit pas dépassé 2Mo, les types de fichier supportés sont : jpg, png, gif."
  							placeholderImage={null}
  							initialValue={config.links.public + '/datas/img/company/axa-group.jpg'}
  							initialValueType="url"
  							isLoading={false}
  							disabled={false}
          					fileInputAction="Uploader un fichier"
          					fileInputDetails="Ajouter un fichier depuis votre ordinateur"
          					urlInputAction="Fournir une url"
          					urlInputDetails="Ajouter un fichier depuis le web"
          					initialUrlInputValue={""}
          					urlInputPlaceholder="http://monsite.com/ma_photo.jpg" />
					</li>
				</ul>

				{/* /PHOTO */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* AUTOCOMPLETE */}

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							type="text"
							placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							type="text"
							placeholder="Placeholder"
							initialInputValue="Anthony Fraing"
							initialValue="option1"
							optionsMaxLength={4}
							options={[
								{ value: "option1", label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: "option2", label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" },
								{ value: "option3", label: "Jean Michel Jaiunnomaralonge", avatar: window.config.links.public + '/datas/img/user/a11.jpg', subLabel: "• Collaborateur" },
							]}
							additionalOptions={[
								{ onClick: function() { console.log("You've push the button!"); }, label: "+ Ajouter un contact", affectedByHighlighter: false },
							]}
							matcher={false}
							highlighter={true}
							validOptionOnly />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							type="text"
							placeholder="Placeholder"
							initialValue="option1"
							options={[
								{ value: "option1", label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: "option2", label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" }
							]}
							readOnly />
					</li>
				</ul>

				<br/>

				<ul className="h-list marged-list">
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							transparent
							type="text"
							placeholder="Placeholder" />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							transparent
							type="text"
							placeholder="Placeholder"
							initialInputValue="Anthony Fraing"
							initialValue="option1"
							optionsMaxLength={4}
							options={[
								{ value: "option1", label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: "option2", label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" },
								{ value: "option3", label: "Jean Michel Jaiunnomaralonge", avatar: window.config.links.public + '/datas/img/user/a11.jpg', subLabel: "• Collaborateur" },
							]}
							additionalOptions={[
								{ onClick: function() { console.log("You've push the button!"); }, label: "+ Ajouter un contact", affectedByHighlighter: false },
							]}
							matcher={false}
							highlighter={true}
							validOptionOnly />
					</li>
					<li style={{ width: '250px' }}>
						<Nextrap.ControlAutocomplete
							transparent
							type="text"
							placeholder="Placeholder"
							initialValue="option1"
							options={[
								{ value: "option1", label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: "option2", label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" }
							]}
							readOnly />
					</li>
				</ul>

				{/* /AUTOCOMPLETE */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* TOOLTIP */}

				<div>

					<Nextrap.Button type="white" large>
						<span>Annuler</span>
						<Nextrap.Tooltip position="afterMiddle" helperPosition="afterMiddle" isInitialyShown><span>Tooltip 1</span></Nextrap.Tooltip>
					</Nextrap.Button>

				</div>

				{/* /TOOLTIP */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* SHARE */}

				<ul className="h-list marged-list">
					<li>
						<Nextrap.ControlShare
							initialValue="SELF"
							modalInitialValue={{
								value: "CUSTOM",
								allow: [
									{ target: { id: 1, avatar: window.config.links.public + '/datas/img/user/a5.jpg', name: "Paul Draner", type: "customer" }, perms: "WRITE" }
								],
								deny: [
									{ id: 4, avatar: window.config.links.public + '/datas/img/user/a6.jpg', name: "Paul Guillotel", type: "customer" }
								]
							}}
							modalAllowInputOptions={[
								{ value: { id: 2, avatar: window.config.links.public + '/datas/img/user/a12.jpg', name: "Anthony Fraing", type: "user" }, label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: { id: 3, avatar: window.config.links.public + '/datas/img/user/a13.jpg', name: "Jean Huit", type: "user" }, label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" }
							]}
							modalDenyInputOptions={[
								{ value: { id: 2, avatar: window.config.links.public + '/datas/img/user/a12.jpg', name: "Anthony Fraing", type: "user" }, label: "Anthony Fraing", avatar: window.config.links.public + '/datas/img/user/a12.jpg', subLabel: "• Collaborateur" },
								{ value: { id: 3, avatar: window.config.links.public + '/datas/img/user/a13.jpg', name: "Jean Huit", type: "user" }, label: "Jean Huit", avatar: window.config.links.public + '/datas/img/user/a13.jpg', subLabel: "• Collaborateur" }
							]} />
					</li>
					<li>
						<Nextrap.ControlShare initialValue={{ value: "WORKSPACE", perms: "READ" }} />
					</li>
					<li>
						<Nextrap.ControlShare initialValue={{ value: "WORKSPACE", perms: "SPEAK" }} />
					</li>
					<li>
						<Nextrap.ControlShare initialValue={{ value: "WORKSPACE", perms: "WRITE" }} />
					</li>
					<li>
						<Nextrap.ControlShare initialValue={{ value: "CUSTOM", allow: [], deny: [] }} />
					</li>
					<li>
						<Nextrap.ControlShare readOnly />
					</li>
				</ul>

				{/* /SHARE */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* CONTROL LABEL */}

				<ul className="v-list marged-list" style={{ background: '#FFF', padding: '20px' }}>
					<li style={{ width: 400 }}>
						<Nextrap.FormGroup>
							<Nextrap.ControlLabel><span>Title</span></Nextrap.ControlLabel>
							<Nextrap.ControlGroup>
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder2" />
							</Nextrap.ControlGroup>
							<Nextrap.ControlGroup>
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder2" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder3" />
							</Nextrap.ControlGroup>
						</Nextrap.FormGroup>
						<Nextrap.FormGroup>
							<Nextrap.ControlLabel><span>Title 2</span></Nextrap.ControlLabel>
							<Nextrap.ControlGroup>
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder2" />
							</Nextrap.ControlGroup>
							<Nextrap.ControlGroup>
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder2" />
								<Nextrap.ControlText transparent type="text" placeholder="Placeholder3" />
							</Nextrap.ControlGroup>
						</Nextrap.FormGroup>
					</li>
				</ul>

				{/* /CONTROL LABEL */}

				<br/>

				<br/>

				<br/>

				<br/>

				{/* TABLE */}

				<table className="table">

					<thead>
						<tr>
							<th>Nom</th>
							<th>Job</th>
							<th>Email</th>
							<th>Phone</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>Paul Duthoit</td>
							<td>CTO chez Nexty</td>
							<td>duthoit.paul@gmail.com</td>
							<td>06.14.32.82.83</td>
						</tr>
					</tbody>

				</table>

				{/* /TABLE */}

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

				<br/>

			</div>
		);
	}
});


// Exports component
module.exports = Ui;
