var React = require('react');


// Set component : Content
var Content = React.createClass({

	// Render
	render: function() {

		// Return
		return (
    		<div id="content">
    			<div className="container">
	    			{this.props.children}
					<Nextrap.ContextualLayerGroup />
				</div>
    		</div>
		);

	}

});



// Exports component
module.exports = Content;
