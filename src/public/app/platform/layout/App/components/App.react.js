var React = require('react');


// Get components
var Content = require('../../Content/components/Content.react');


// Set component
var App = React.createClass({

	// Render
	render: function() {

		// Return
		return (
			<div id="nextyApp">
				<Content>{this.props.children}</Content>
				<Nextrap.ContextualLayerGroup />
				<Nextrap.ContextualWindowGroup />
			</div>
		);

	}

});


// Exports component
module.exports = App;