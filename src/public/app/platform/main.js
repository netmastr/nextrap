var React = require('react');
var ReactDOM = require('react-dom');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var IndexRedirect = require('react-router').IndexRedirect;
var BrowserHistory = require('react-router').browserHistory;
var Moment = require('moment');


// Init locales
require('../../resources/intl/en_moment.js')(Moment);
require('../../resources/intl/fr_moment.js')(Moment);


// Get nextrap
var Nextrap = require('../../../../lib/nextrap');
window.Nextrap = Nextrap;



// Set routes
var RouterConfig = (
	<Router history={BrowserHistory}>
		<Route path="/" component={require('./handlers/App.react')}>
	
			<IndexRedirect to="/ui" />

			<Route name="ui" path="ui" component={require('./handlers/Ui.react')} />

		</Route>
	</Router>
);



// Set intl data
var intlData = {
    locales: ['fr-FR'],
    messages: require('../../resources/intl/fr-FR_messages')
};


// Set locales to Moment
Moment.locale(intlData.locales);
Moment.relativeTimeThreshold('s', 59);
Moment.relativeTimeThreshold('m', 59);
Moment.relativeTimeThreshold('h', 23);
Moment.relativeTimeThreshold('d', 25);
Moment.relativeTimeThreshold('M', 11);

 
// Run router
//ReactDOM.render(RouterConfig, function(Handler) {
//	React.render(<Handler {...intlData} />, document.body);
//});

 
// Run router
ReactDOM.render(RouterConfig, window.document.getElementById('app'));