module.exports = {
	entity: {
		type: '{type, select,' +
			'customer {Client}' +
			'provider {Fournisseur}' +
	        'other {}' +
		'}',
	    phoneType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'cell {Mobile}' +
			'second {Secondaire}' +
	        'other {}' +
		'}',
	    emailType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'second {Secondaire}' +
	        'other {}' +
		'}',
		addressType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'billing {Facturation}' +
			'headquarter {Siège social}' +
	       ' other {}' +
		'}',
	},
	keywords: {
		at: 'Chez',
		workAt: 'Travaille chez',
	},
	actions: {
		like: 'J\'aime',
		hasLike: 'A aimé',
		comment: 'Commenter',
		hasComment: 'A commenté',
		post: 'Poster',
		hasPost: 'A posté'
	},
	post: {
		likesCount: '{otherLikes, plural,' +
			'=0 {{likedByMe, select,' +
				'yes {Vous aimez}' +
				'other {}' +
			'}}' +
			'=1 {{likedByMe, select,' +
				'yes {Vous et une personne aimez}' +
				'other {Une personne aime}' +
			'}}' +
	        'other {{likedByMe, select,' +
				'yes {Vous et {otherLikes} personnes aimez}' +
				'other {{otherLikes} personnes aiment}' +
			'}}' +
		'}',
		commentsCount: '{itemCount, plural,' +
			'=0 {}' +
			'=1 {Un commentaire}' +
	        'other {# commentaires}' +
		'}'
	}
};