/*
* auth.js
*
* Auth middlewares
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var _ = require('underscore');



// Populate method
var populate = function(req, res, next) {

	// Datas
	var session = req.session;

	// Set auth
	if(!_.isObject(session) || _.isEmpty(session.auth)) {
		global.__auth = null;
	}

	else {
		global.__auth = session.auth;
	}

	// Next
	next();
	return;

};



// Exports module
module.exports = {
	populate: populate
};