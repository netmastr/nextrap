/*
* app.js
*
* App handler
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/



// Ping method
var ping = function(req, res, next) {

	// Send
	res.send('pong');
	return;

};


// Index method
var index = function(req, res, next) {

	// Render index
	res.render('index');
	return;

};



// Exports module
module.exports = {
	ping: ping,
	index: index
};