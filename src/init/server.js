/*
* init/server.js
*
* Init server
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/



// Exports init
module.exports.init = function() {

	// Datas
	var port = process.env.NODE_PORT || 3004;

	// Start http server
	global.__server = __app.listen(port);

	// Set globals self links
	global.__config.links.self = __config.links.self + ":" + port;
	global.__absoluteRoot = __absoluteRoot + ":" + port;

	// Bind server errors
	__server.on('error', function(error) {

		// Log error
		console.log("Error : ", error);

	});

	// Log server start
	console.log("Environment : " + __config.env);
	console.log("Server running at " + __absoluteRoot);

};