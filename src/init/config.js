/*
* config.js
*
* Init config
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var path = require('path');
var config = require('../config/cursor');



// Exports init
module.exports.init = function() {

	// Set config global
	global.__config = config;

	// Set default globals
	global.__documentRoot = path.resolve(__dirname, '../');
	global.__absoluteRoot = config.links.self;
	global.__root = '/';
	global.__public = config.links.public;
	global.__views = config.links.views;
	global.__datas = config.links.datas;
	global.__bundles = config.links.bundles;
	global.__resources = config.links.resources;

};