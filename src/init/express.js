/*
* express.js
*
* Init express
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var cors = require('cors');
var path = require('path');



// Exports init
module.exports.init = function() {

	// Variables
	__app.set('showStackError', true);
	__app.set('root', __root);
	__app.set('env', __config.env);
	__app.set('views', path.join(__documentRoot, __views));
	__app.set('view engine', 'ejs')

	// Default middlewares
	__app.use(bodyParser.urlencoded({ extended: true }));
	__app.use(bodyParser.json());
	__app.use(multipart());
	__app.use(cors());

	// Session middlewares
	__app.use(session({ secret: 'nextyprotection', resave: true, saveUninitialized: true }));

	// Static middlewares
	__app.use(__public, express.static(path.join(__documentRoot, __public)));
	__app.use(__public, function(req, res, next) { res.status(404).end(); });

};
