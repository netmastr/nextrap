/*
* init/routes.js
*
* Init routes
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var appHandler = require('../handlers/app');


// Exports init
module.exports.init = function() {

	// App routes
	__app.get('/ping', appHandler.ping);
	__app.use(appHandler.index);

};