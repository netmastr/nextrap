/*
* app.js
*
* Main file
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var express = require('express');


// Set global app
global.__app = express();


// Log start
console.log('Starting nextrap...');


// Initialisation
require('./init/config').init();
require('./init/express').init();
require('./init/routes').init();
require('./init/server').init();