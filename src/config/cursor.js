/*
* cursor.js
*
* Cursor configuration file for environment variables
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/

// Dependencies
var developmentConfig = require('./development');
var productionConfig = require('./production');


// Datas
var env = process.env.NODE_ENV || 'development';
var config = {};


// Get config
switch(env) {

	// Development
	case 'development':
		config = developmentConfig;
		break;

	// Production
	case 'production':
		config = productionConfig;
		break;

}


// Exports module
module.exports = config;