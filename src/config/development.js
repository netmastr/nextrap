/*
* development.js
*
* Global development configuration file
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/



// Datas
var config = {};


// Set config
config.env = 'development';
config.defaultTimeZoneName = 'Europe/Paris';

// Set config links
config.links = {}
config.links.self = 'http://localhost';
config.links.api = 'http://localhost:3000';
config.links.public = '/public';
config.links.views = '/views';
config.links.datas = config.links.public + '/datas';
config.links.bundles = config.links.public + '/bundles';
config.links.resources = config.links.public + '/resources';


// Exports module
module.exports = config;
