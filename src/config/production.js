/*
* production.js
*
* Global production configuration file
*
* Author: Paul Duthoit
* Copyright(c) 2015 NEXTY
*/



// Datas
var config = {};


// Set config
config.env = 'production';
config.defaultTimeZoneName = 'Europe/Paris';

// Set config links
config.links = {}
config.links.self = 'https://nextycloud.com';
config.links.api = 'https://api.nextycloud.com';
config.links.public = '/public';
config.links.views = '/views';
config.links.datas = config.links.public + '/datas';
config.links.bundles = config.links.public + '/bundles';
config.links.resources = config.links.public + '/resources';


// Exports module
module.exports = config;
