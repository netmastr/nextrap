module.exports = function(grunt) {
 
	// Grunt init configuration
	grunt.initConfig({

		// Grunt clean
		clean: {
			prepare: {
				src: [ 'build' ]
			}
		},

		// Grunt mkdir
		mkdir: {
			prepare: {
				options: {
					create: [ 'build/public/bundles' ]
				}
			}
		},

		// Grunt copy
		copy: {
			prepare: {
				expand: true,
				cwd: 'src',
				src: [ '**', '!public/resources/sass/**', '!public/app/**' ],
				dest: 'build'
			}
		},
 
		// Grunt sass
		sass: {
			compile: {
				options: {
					style: 'expanded'
				},
				files: [
					{
						expand: true,
						cwd: 'src/public/resources/sass',
						src: [ '**/*.scss' ],
						dest: 'build/public/resources/css',
						ext: '.css'
					},
					{
						expand: true,
						cwd: 'lib/nextrap/scss',
						src: [ 'nextrap.scss' ],
						dest: 'lib/nextrap/css',
						ext: '.css'
					}
				]
			}
		},

		// Grunt autoprefixer
		autoprefixer: {
			build: {
				expand: true,
				cwd: 'lib/nextrap/css',
				src: [ 'nextrap.css' ],
				dest: 'lib/nextrap/css'
			}
		},

		// Grunt browserify and reactify
		browserify: {
			build: {
				src: 'src/public/app/platform/main.js',
				dest: 'build/public/bundles/platform.js'
				/*
				src: [
					'src/public/app/app.js',
					'src/public/app/dependencies.js',
				]
				dest: 'build/public/bundles/common.js'
				options:
					plugin: [
						[ 'factor-bundle', { o: [ 'build/public/bundles/app.js', 'build/public/bundles/dependencies.js' ] } ]
					]
				*/
			}
		},

		// Grunt minify css
		cssmin: {
			release: {
				expand: true,
				cwd: 'lib/nextrap/css',
				src: [ 'nextrap.css' ],
				dest: 'lib/nextrap/css',
			}
		},

		// Grunt minify js
		uglify: {
			release: {
				expand: true,
				cwd: 'build/public',
				src: [ '**/*.js' ],
				dest: 'build/public'
			}
		}

	});
 
	// Load plugins
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-mkdir');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-browserify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
 
	// Tasks
	grunt.registerTask('release', [ 'clean', 'mkdir', 'copy', 'sass', 'autoprefixer', 'uglify', 'browserify' ]);
	grunt.registerTask('build', [ 'clean', 'mkdir', 'copy', 'sass', 'browserify' ]);

}
