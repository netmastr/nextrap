var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : Avatar
var Avatar = React.createClass({

	// Prop types
	propTypes: {
		size: React.PropTypes.string,
		href: React.PropTypes.string,
		hrefTitle: React.PropTypes.string,
		imageSrc: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.arrayOf(React.PropTypes.string)
		]),
		inline: React.PropTypes.bool,
		asBackground: React.PropTypes.bool
	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'size', 'href', 'hrefTitle', 'imageSrc', 'inline', 'asBackground', 'className' ]);
	},

	// Render
	render: function() {

		// Data
		var component = false;
		var componentAttributes = this._getComponentAttributes(this.props);
		var size = this.props.size || 'xs';
		var href = this.props.href || null;
		var hrefTitle = this.props.hrefTitle || null;
		var imageSrc = this.props.imageSrc || null;
		var isInline = !!this.props.inline;
		var asBackground = !!this.props.asBackground;
		var multi = !!(imageSrc && imageSrc instanceof Array && imageSrc.length > 1);
		var classes = classNames('avatar', ('avatar-' + size), { multi: multi }, this.props.className);

		// Add image
		if(multi) {
			component = (
				<div className='multi-frame'>
					<ul className={classNames('area-' + _.min([ imageSrc.length, 4 ]))}>
						{_.map(imageSrc, function(srcItem, srcIndex) {
							if(srcItem && asBackground) {
								return <li key={srcIndex}><div className='background-img' style={{ backgroundImage: 'url(' + srcItem + ')' }} /></li>;
							} else if(srcItem) {
								return <li key={srcIndex}><img src={srcItem} /></li>;
							} else {
								return <li key={srcIndex}><div className='empty-area' /></li>;
							}
						})}
					</ul>
				</div>
			);
		} else if(imageSrc && asBackground) {
			component = <div className='background-img' style={{ backgroundImage: 'url(' + imageSrc + ')' }} />;
		} else if(imageSrc) {
			component = <img src={imageSrc} />;
		}

		// Add link
		if(href && hrefTitle) {
			component = <a href={href} title={hrefTitle}>{component}</a>;
		} else if(href) {
			component = <a href={href}>{component}</a>;
		}

		// Add frame
		if(isInline) {
			component = <span {...componentAttributes} className={classes}>{component}</span>;
		} else {
			component = <div {...componentAttributes} className={classes}>{component}</div>;
		}

		// Return component
		return component;

	}
	
});


// Exports component
module.exports = Avatar;
