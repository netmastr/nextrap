var React = require('react');
var ReactRouter = require('react-router-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : Button
var Button = React.createClass({

	// Prop types
	propTypes: {

		// Important
		type: React.PropTypes.string,
		small: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		loading: React.PropTypes.bool,
		dropdown: React.PropTypes.bool,
		active: React.PropTypes.bool,
		colored: React.PropTypes.bool,
		externalLink: React.PropTypes.bool,

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'type', 'small', 'disabled', 'readOnly', 'loading', 'dropdown', 'active', 'colored', 'href', 'externalLink', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var href = this.props.href;
		var isLink = !!this.props.href;
		var externalLink = !!this.props.externalLink;
		var children = this.props.children;
		var classes = classNames(
			"btn",
			this.props.type ? "btn-" + this.props.type : null,
			{
				small: this.props.small,
				disabled: this.props.disabled,
				'read-only': this.props.readOnly,
				active: this.props.active,
				loading: this.props.loading,
				'btn-dropdown': this.props.dropdown,
				colored: this.props.colored
			},
			this.props.className
		);

		// If link
		if(isLink && externalLink) {
			return(
				<a href={href} {...componentAttributes} className={classes}>
					{this.props.children}
				</a>
			);
		}

		// If link
		else if(isLink) {
			return(
				<ReactRouter.Link to={href} {...componentAttributes} className={classes}>
					{this.props.children}
				</ReactRouter.Link>
			);
		}

		// If button
		else {
			return(
				<div {...componentAttributes} className={classes}>
					{this.props.children}
				</div>
			);
		}

	}

});


// Exports component
module.exports = Button;