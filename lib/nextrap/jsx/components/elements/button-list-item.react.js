var React = require('react');
var ReactRouter = require('react-router-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ButtonListItem
var ButtonListItem = React.createClass({

	// Prop types
	propTypes: {

		// Important
		loading: React.PropTypes.bool,
		active: React.PropTypes.bool,
		readonly: React.PropTypes.bool,
		externalLink: React.PropTypes.bool

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'loading', 'href', 'active', 'readonly', 'externalLink', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var href = this.props.href;
		var isLink = !!this.props.href;
		var externalLink = !!this.props.externalLink;
		var children = this.props.children;
		var classes = classNames("btn", "btn-list-item", {
			loading: this.props.loading,
			active: this.props.active,
			readonly: this.props.readonly
		}, this.props.className);

		// If link
		if(isLink && externalLink) {
			return(
				<a href={href} {...componentAttributes} className={classes}>
					{this.props.children}
				</a>
			);
		}

		// If link
		else if(isLink) {
			return(
				<ReactRouter.Link to={href} {...componentAttributes} className={classes}>
					{this.props.children}
				</ReactRouter.Link>
			);
		}

		// If button
		else {
			return(
				<div {...componentAttributes} className={classes}>
					{this.props.children}
				</div>
			);
		}

	}

});


// Exports component
module.exports = ButtonListItem;