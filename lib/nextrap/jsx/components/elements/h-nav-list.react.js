var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : HNavList
var HNavList = React.createClass({

	// Prop types
	propTypes: {

		// Important
		dark: React.PropTypes.bool,
		withoutBorder: React.PropTypes.bool

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'dark', 'withoutBorder', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var children = this.props.children;
		var classes = classNames("h-list", "h-nav-list", {
			"dark": this.props.dark,
			"without-border": this.props.withoutBorder
		}, this.props.className);

		// Return
		return(
			<ul className={classes}>
				{this.props.children}
			</ul>
		);

	}

});


// Exports component
module.exports = HNavList;