var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : Table
var Table = React.createClass({

	// Prop types
	propTypes: {},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var children = this.props.children;
		var classes = classNames("table", this.props.className);

		// Return
		return(
			<table {...componentAttributes} className={classes}>
				{this.props.children}
			</table>
		);

	}

});


// Exports component
module.exports = Table;