var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Get config
var config = require('../../config');


// Set component : Email
var Email = React.createClass({

	// Prop types
	propTypes: {
		street: React.PropTypes.string,
		complement: React.PropTypes.string,
		zip: React.PropTypes.string,
		city: React.PropTypes.string,
		region: React.PropTypes.string,
		country: React.PropTypes.string,
		placeholder: React.PropTypes.string
	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'street', 'complement', 'zip', 'city', 'region', 'country', 'placeholder', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var street = this.props.street || null;
		var complement = this.props.complement || null;
		var zip = this.props.zip || null;
		var city = this.props.city || null;
		var region = this.props.region || null;
		var country = this.props.country || null;
		var placeholder = this.props.placeholder || null;
		var classes = classNames('formatted-address', this.props.className);

		// If empty
		if(!(street || complement || zip || city || region || country)) {
			if(placeholder) return <span {...componentAttributes} className={classes}>-</span>;
			else return <noscript/>;
		}

		// Return
		return (
			<span {...componentAttributes} className={classes}>
				{(function() {

					// Set data
					var addressItems = [];

					// Add street
					if(street) {
						addressItems.push(<span key={0} className='formatted-address-street'>{street}</span>);
					}

					// Add complement
					if(complement) {
						addressItems.push(<span key={1} className='formatted-address-complement'>{complement}</span>);
					}

					// Add city
					if(zip || city) {
						addressItems.push(
							<span key={2}>
								{_.reduce([
									(zip ? <span key={0} className='formatted-address-zip'>{zip}</span> : null),
									(city ? <span key={1} className='formatted-address-city'>{city}</span> : null)
								], function(memo, obj) {
									if(obj && memo.length > 0) memo.push(<span key={memo.length + '.' + 0}> </span>);
									if(obj) memo.push(obj);
									return memo;
								}, [])}
							</span>
						);
					}

					// Add country
					if(region || country) {
						addressItems.push(
							<span key={3}>
								{_.reduce([
									(region ? <span key={0} className='formatted-address-region'>{region}</span> : null),
									(country ? <span key={1} className='formatted-address-country'>{country}</span> : null)
								], function(memo, obj) {
									if(obj && memo.length > 0) memo.push(<span key={memo.length + '.' + 0}> </span>);
									if(obj) memo.push(obj);
									return memo;
								}, [])}
							</span>
						);
					}

					// Return items
					return _.reduce(addressItems, function(memo, obj) {
						if(memo.length > 0) memo.push(<br key={memo.length + '.' + 0}/>);
						memo.push(obj);
						return memo;
					}, []);

				})()}
			</span>
		);

	}
	
});


// Exports component
module.exports = Email;