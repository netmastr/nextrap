var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : VNavList
var VNavList = React.createClass({

	// Prop types
	propTypes: {},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var children = this.props.children;
		var classes = classNames("v-nav-list", this.props.className);

		// Return
		return(
			<div className={classes}>
				{this.props.children}
			</div>
		);

	}

});


// Exports component
module.exports = VNavList;