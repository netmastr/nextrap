var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ButtonList
var ButtonList = React.createClass({

	// Prop types
	propTypes: {

		// Important
		small: React.PropTypes.bool,
		separatedByBorder: React.PropTypes.bool

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'small', 'separatedByBorder', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var children = this.props.children;
		var classes = classNames('btn-list', { small: this.props.small, 'separated-by-border': this.props.separatedByBorder }, this.props.className);

		// Return
		return(
			<div className={classes}>
				{this.props.children}
			</div>
		);

	}

});


// Exports component
module.exports = ButtonList;