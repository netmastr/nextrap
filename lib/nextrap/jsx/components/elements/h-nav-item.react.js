var React = require('react');
var ReactRouter = require('react-router-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : HNavItem
var HNavItem = React.createClass({

	// Prop types
	propTypes: {

		// Important
		active: React.PropTypes.bool,
		externalLink: React.PropTypes.bool,

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'active', 'href', 'externalLink', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var href = this.props.href;
		var isLink = !!this.props.href;
		var externalLink = !!this.props.externalLink;
		var children = this.props.children;
		var classes = classNames("btn", "h-nav-item", {
			active: this.props.active
		}, this.props.className);

		// If link
		if(isLink && externalLink) {
			return(
				<li>
					<a href={href} {...componentAttributes} className={classes}>
						{this.props.children}
					</a>
				</li>
			);
		}

		// If link
		else if(isLink) {
			return(
				<li>
					<ReactRouter.Link to={href} {...componentAttributes} className={classes}>
						{this.props.children}
					</ReactRouter.Link>
				</li>
			);
		}

		// If button
		else {
			return(
				<li>
					<div {...componentAttributes} className={classes}>
						{this.props.children}
					</div>
				</li>
			);
		}

	}

});


// Exports component
module.exports = HNavItem;