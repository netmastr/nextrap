var React = require('react');


// Set component : Link
var Link = React.createClass({

	// Render
	render: function() {

		// Datas
		var label = this.props.label || null;
		var href = this.props.href || null;
		var linked = !!this.props.linked;
		var external = !!this.props.external;

		// Return
		if(href && linked && label) {
			return (
				<span className="formatted-link"><a href={href} title={label} target={external ? "_blink" : null}>{label}</a></span>
			);
		} else if(href && linked && !label) {
			return (
				<span className="formatted-link"><a href={href} title={href} target={external ? "_blink" : null}>{href}</a></span>
			);
		} else if(href && !linked && !label) {
			return (
				<span className="formatted-link">{href}</span>
			);
		}

		// If empty
		else {
			return false;
		}

	}
	
});


// Exports component
module.exports = Link;