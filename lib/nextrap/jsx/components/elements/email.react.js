var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');
var IntlMessageFormat = require('intl-messageformat');


// Get config
var config = require('../../config');


// Set component : Email
var Email = React.createClass({

	// Prop types
	propTypes: {
		address: React.PropTypes.string,
		type: React.PropTypes.string,
		linked: React.PropTypes.bool
	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'address', 'type', 'linked', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var address = this.props.address || null;
		var type = this.props.type ? new IntlMessageFormat(config.localeMessages.entity.emailType, config.locale).format({ type: this.props.type.toLowerCase() }) : null;
		var linked = !!this.props.linked;
		var classes = classNames("formatted-email", this.props.className);

		// If address and type
		if(address && type && linked) {
			return (
				<span {...componentAttributes} className={classes}>
					<span className="formatted-email-address"><a href={"mailto:" + address}>{address}</a></span>
					<span className="formatted-email-type">{type}</span>
				</span>
			);
		} else if(address && type && !linked) {
			return (
				<span {...componentAttributes} className={classes}>
					<span className="formatted-email-address">{address}</span>
					<span className="formatted-email-type">{type}</span>
				</span>
			);
		}

		// If only address
		else if(address && !type && linked) {
			return (
				<span {...componentAttributes} className={classes}>
					<span className="formatted-email-address"><a href={"mailto:" + address}>{address}</a></span>
				</span>
			);
		} else if(address && !type && !linked) {
			return (
				<span {...componentAttributes} className={classes}>
					<span className="formatted-email-address">{address}</span>
				</span>
			);
		}

		// If empty
		else {
			return <noscript/>;
		}

	}
	
});


// Exports component
module.exports = Email;