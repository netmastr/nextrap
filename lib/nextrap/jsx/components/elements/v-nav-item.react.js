var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : VNavItem
var VNavItem = React.createClass({

	// Prop types
	propTypes: {

		// Important
		loading: React.PropTypes.bool,
		active: React.PropTypes.bool,
		readonly: React.PropTypes.bool

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'loading', 'active', 'readonly', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var isLink = !!this.props.href;
		var children = this.props.children;
		var classes = classNames("btn", "v-nav-item", {
			loading: this.props.loading,
			active: this.props.active,
			readonly: this.props.readonly
		}, this.props.className);

		// If link
		if(isLink) {
			return(
				<a {...componentAttributes} className={classes}>
					{this.props.children}
				</a>
			);
		}

		// If button
		else {
			return(
				<div {...componentAttributes} className={classes}>
					{this.props.children}
				</div>
			);
		}

	}

});


// Exports component
module.exports = VNavItem;