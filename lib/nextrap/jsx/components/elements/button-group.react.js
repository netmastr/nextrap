var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ButtonGroup
var ButtonGroup = React.createClass({

	// Prop types
	propTypes: {},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var children = this.props.children;
		var classes = classNames("btn-group", this.props.className);

		// Return
		return(
			<div {...componentAttributes} className={classes}>
				{this.props.children}
			</div>
		);

	}

});


// Exports component
module.exports = ButtonGroup;