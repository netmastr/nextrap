var React = require('react');
var IntlMessageFormat = require('intl-messageformat');


// Get config
var config = require('../../config');


// Set component : Phone
var Phone = React.createClass({

	// Render
	render: function() {

		// Datas
		var number = this.props.number || null;
		var type = this.props.type ? new IntlMessageFormat(config.localeMessages.entity.phoneType, config.locale).format({ type: this.props.type.toLowerCase() }) : null;
		var linked = !!this.props.linked;

		// If number and type
		if(number && type && linked) {
			return (
				<span className="formatted-phone">
					<span className="formatted-phone-number"><a href={"tel:" + number}>{number}</a></span>
					<span className="formatted-phone-type">{type}</span>
				</span>
			);
		} else if(number && type && !linked) {
			return (
				<span className="formatted-phone">
					<span className="formatted-phone-number">{number}</span>
					<span className="formatted-phone-type">{type}</span>
				</span>
			);
		}

		// If only number
		else if(number && !type && linked) {
			return (
				<span className="formatted-phone">
					<span className="formatted-phone-number"><a href={"tel:" + number}>{number}</a></span>
				</span>
			);
		} else if(number && !type && !linked) {
			return (
				<span className="formatted-phone">
					<span className="formatted-phone-number">{number}</span>
				</span>
			);
		}

		// If empty
		else {
			return false;
		}

	}
	
});


// Exports component
module.exports = Phone;