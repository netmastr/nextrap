var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');
var IntlMessageFormat = require('intl-messageformat');


// Get config
var config = require('../../config');


// Set component : Job
var Job = React.createClass({

	// Prop types
	propTypes: {
		job: React.PropTypes.string,
		companyName: React.PropTypes.string,
		companyHref: React.PropTypes.string
	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'job', 'companyName', 'companyHref', 'className' ]);
	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var job = this.props.job || null;
		var companyName = this.props.companyName || null;
		var companyHref = this.props.companyHref || null;

		// If job and company
		if(job && companyName && companyHref) {
			return (
				<span className={classNames(this.props.className)}>
					<span className="formatted-job">{job}</span>
					<span className="subtle">{" " + new IntlMessageFormat(config.localeMessages.keywords.at, config.locale).format().toLowerCase() + " "}</span>
					<span className="formatted-company"><a href={companyHref} title={companyName}>{companyName}</a></span>
				</span>
			);
		} else if(job && companyName && !companyHref) {
			return (
				<span className={classNames(this.props.className)}>
					<span className="formatted-job">{job}</span>
					<span className="subtle">{" " + new IntlMessageFormat(config.localeMessages.keywords.at, config.locale).format().toLowerCase() + " "}</span>
					<span className="formatted-company">{companyName}</span>
				</span>
			);
		}

		// If only job
		else if(job && !companyName && !companyHref) {
			return (
				<span className={classNames('formatted-job', this.props.className)}>{job}</span>
			);
		}

		// If only company
		else if(!job && companyName && companyHref) {
			return (
				<span className={classNames(this.props.className)}>
					<span className="subtle">{new IntlMessageFormat(config.localeMessages.keywords.workAt, config.locale).format() + " "}</span>
					<span className="formatted-company"><a href={companyHref} title={companyName}>{companyName}</a></span>
				</span>
			);
		} else if(!job && companyName && !companyHref) {
			return (
				<span className={classNames(this.props.className)}>
					<span className="subtle">{new IntlMessageFormat(config.localeMessages.keywords.workAt, config.locale).format() + " "}</span>
					<span className="formatted-company">{companyName}</span>
				</span>
			);
		}

		// If empty
		else {
			return <noscript/>;
		}

	}
	
});


// Exports component
module.exports = Job;