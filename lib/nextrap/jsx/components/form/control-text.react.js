var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : Text
var Text = React.createClass({

	// Instance data
	_input: null,

	// Prop types
	propTypes: {

		// Important
		type: React.PropTypes.string.isRequired,
		name: React.PropTypes.string,
		value: React.PropTypes.string,
		initialValue: React.PropTypes.string,
		placeholder: React.PropTypes.string,
		transparent: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		before: React.PropTypes.string,
		after: React.PropTypes.string,
		inputProps: React.PropTypes.object,
		inputRef: React.PropTypes.func,
		component: React.PropTypes.func,

		// Plus
		className: React.PropTypes.string,
		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func,
		onEnterKeyPress: React.PropTypes.func,
		blurOnEnterKeyPress: React.PropTypes.bool

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.value === 'undefined' && typeof this.props.initialValue !== 'undefined') {
			initialState.value = this.props.initialValue || '';
		} else if(typeof this.props.value === 'undefined') {
			initialState.value = '';
		}

		// Return state
		return initialState;

	},

	// After component mount
	componentDidMount: function () {

		// Return component
		if(_.isFunction(this.props.component)) {
			this.props.component(this);
		}
	      
	},

	// After component mount
	componentDidUpdate: function () {

		// Return component
		if(_.isFunction(this.props.component)) {
			this.props.component(this);
		}
	      
	},

	// Get interactive props
	_getInteractiveData: function() {

		// Data
		var interactiveData = {};

		// If uncontrolled data
		if(typeof this.props.value !== 'undefined') {
			interactiveData.value = this.props.value || '';
		} else {
			interactiveData.value = this.state.value;
		}

		// Return controlled props
		return interactiveData;

	},

	// Render
	render: function() {

		// Data
		var self = this;
		var interactiveData = this._getInteractiveData();
		var value = interactiveData.value;
		var type = this.props.type;
		var name = this.props.name || null;
		var placeholder = this.props.placeholder || null;
		var readOnly = !!this.props.readOnly;
		var before = this.props.before;
		var after = this.props.after;
		var inputProps = this.props.inputProps || {};
		var classes = classNames('form-control', 'form-text', this.props.className, {
			'read-only': readOnly,
			transparent: this.props.transparent
		});
			
		// Return
		return(
			<div className={classes}>
				<div className='t-group'>

					{ before
						? (
							<div className='t-cell before-cell'>
								<span className='subtle nowrap'>{before}&nbsp;</span>
							</div>
						)
						: null
					}

					<div className='t-cell input-cell max-width'>
						{ _.indexOf([ 'text', 'email', 'number', 'password', 'phone', 'url' ], type) !== -1
							? (
								<input
									{...inputProps}
									type={type}
									name={name}
									placeholder={placeholder}
									value={value}
									readOnly={readOnly}
									onChange={this._onChange}
									onFocus={this._onFocus}
									onBlur={this._onBlur}
									onKeyPress={this._onKeyPress}
									ref={this._onReference} />
							)
							: null
						}
						{ _.indexOf([ 'textarea' ], type) !== -1
							? (
								<textarea
									{...inputProps}
									name={name}
									placeholder={placeholder}
									value={value}
									readOnly={readOnly}
									onChange={this._onChange}
									onFocus={this._onFocus}
									onBlur={this._onBlur}
									onKeyPress={this._onKeyPress}
									ref={this._onReference} />
							)
							: null
						}
					</div>

					{ after
						? (
							<div className='t-cell after-cell'>
								<span className='subtle nowrap'>&nbsp;{after}</span>
							</div>
						)
						: null
					}

				</div>
			</div>
		);

	},

	// On reference
	_onReference: function(c) {

		// Set input ref
		this._input = c;

		// Reference
		if(_.isFunction(this.props.inputRef)) {
			this.props.inputRef(c);
		}

	},

	// On change
	_onChange: function(e) {

		// Check if read only
		if(this.props.readOnly) {
			return;
		}

		// Datas
		var value = e.currentTarget.value || '';

		// If uncontrolled data
		if(typeof this.props.value === 'undefined') {

			// Set state
			this.setState({
				value: value
			});

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(value, e);
		}

	},

	// On focus function
	_onFocus: function(e) {

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

	},

	// On blur function
	_onBlur: function(e) {

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

	},

	// On key press
	_onKeyPress: function(e) {

		// If pressed key is ENTER
		if(e.which === 13) {

			// EnterKeyPress
			if(_.isFunction(this.props.onEnterKeyPress)) {
				this.props.onEnterKeyPress(e);
			}

			// Blur
			if(this.props.blurOnEnterKeyPress) {
				this._input.blur();
			}

		}

	},

	// Change
	change: function(value, willPropagate) {

		// If uncontrolled data
		if(typeof this.props.value === 'undefined') {

			// Set state
			this.setState({
				value: value
			});

		}

		// Change
		if(willPropagate && _.isFunction(this.props.onChange)) {
			this.props.onChange(value, e);
		}

	}

});


// Exports component
module.exports = Text;