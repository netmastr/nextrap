var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : FormGroup
var FormGroup = React.createClass({

	// Prop types
	propTypes: {},

	// Render
	render: function() {

		// Data
		var classes = classNames("form-group", this.props.className);
			
		// Return
		return(
			<div className={classes}>
				{this.props.children}
			</div>
		);

	}

});


// Exports component
module.exports = FormGroup;