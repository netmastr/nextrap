var React = require('react');
var _ = require('underscore');
var classNames = require('classnames');
var deepcopy = require('deepcopy');


// Get components
var Button = require('../elements/button.react');
var ButtonList = require('../elements/button-list.react');
var ButtonListItem = require('../elements/button-list-item.react');
var Avatar = require('../elements/avatar.react');
var Dropdown = require('../contextual/dropdown.react');
var Modal = require('../contextual/modal.react');
var ControlAutocomplete = require('./control-autocomplete.react');
var ControlSelect = require('./control-select.react');


// Set component : FormShareButton
var FormShareButton = React.createClass({

	// Prop types
	propTypes: {

		// Props
		type: React.PropTypes.string,
		value: React.PropTypes.any,
		initialValue: React.PropTypes.any,
		disabled: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		dropdownPosition: React.PropTypes.string,

		// Events
		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func,
		isFocus: React.PropTypes.bool,
		isInitialyFocus: React.PropTypes.bool,

		// Modal props
		modalValue: React.PropTypes.any,
		modalInitialValue: React.PropTypes.any,
		modalAllowInputOptions: React.PropTypes.array,
		modalAllowValue: React.PropTypes.string,
		modalInitialAllowValue: React.PropTypes.string,
		modalAllowInputValue: React.PropTypes.string,
		modalInitialAllowInputValue: React.PropTypes.string,
		modalDenyInputOptions: React.PropTypes.array,
		modalDenyValue: React.PropTypes.string,
		modalInitialDenyValue: React.PropTypes.string,
		modalDenyInputValue: React.PropTypes.string,
		modalInitialDenyInputValue: React.PropTypes.string,

		// Modal events
		modalOnChange: React.PropTypes.func,
		modalOnOpen: React.PropTypes.func,
		modalOnClose: React.PropTypes.func,
		modalIsOpen: React.PropTypes.bool,
		modalIsInitialyOpen: React.PropTypes.bool,
		modalOnAllowInputFocus: React.PropTypes.func,
		modalOnAllowInputBlur: React.PropTypes.func,
		modalOnAllowInputChange: React.PropTypes.func,
		modalOnDenyInputFocus: React.PropTypes.func,
		modalOnDenyInputBlur: React.PropTypes.func,
		modalOnDenyInputChange: React.PropTypes.func

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.value === "undefined" && typeof this.props.initialValue !== "undefined") {
			initialState.value = deepcopy(this.props.initialValue) || "SELF";
		} else if(typeof this.props.value === "undefined") {
			initialState.value = "SELF";
		}
		if(typeof this.props.isFocus === "undefined" && typeof this.props.isInitialyFocus !== "undefined") {
			initialState.isFocus = !!this.props.isInitialyFocus;
		} else if(typeof this.props.isFocus === "undefined") {
			initialState.isFocus = false;
		}
		if(typeof this.props.modalValue === "undefined" && typeof this.props.modalInitialValue !== "undefined") {
			initialState.modalValue = deepcopy(this.props.modalInitialValue) || { value: "CUSTOM", allow: [], deny: [] };
		} else if(typeof this.props.modalValue === "undefined") {
			initialState.modalValue = { value: "CUSTOM", allow: [], deny: [] };
		}
		if(typeof this.props.modalAllowValue === "undefined" && typeof this.props.modalInitialAllowValue !== "undefined") {
			initialState.modalAllowValue = this.props.modalInitialAllowValue || "";
		} else if(typeof this.props.modalAllowValue === "undefined") {
			initialState.modalAllowValue = "";
		}
		if(typeof this.props.modalAllowInputValue === "undefined" && typeof this.props.modalInitialAllowInputValue !== "undefined") {
			initialState.modalAllowInputValue = this.props.modalInitialAllowInputValue || "";
		} else if(typeof this.props.modalAllowInputValue === "undefined") {
			initialState.modalAllowInputValue = "";
		}
		if(typeof this.props.modalDenyValue === "undefined" && typeof this.props.modalInitialDenyValue !== "undefined") {
			initialState.modalDenyValue = this.props.modalInitialDenyValue || "";
		} else if(typeof this.props.modalDenyValue === "undefined") {
			initialState.modalDenyValue = "";
		}
		if(typeof this.props.modalDenyInputValue === "undefined" && typeof this.props.modalInitialDenyInputValue !== "undefined") {
			initialState.modalDenyInputValue = this.props.modalInitialDenyInputValue || "";
		} else if(typeof this.props.modalDenyInputValue === "undefined") {
			initialState.modalDenyInputValue = "";
		}
		if(typeof this.props.modalIsOpen === "undefined" && typeof this.props.modalIsInitialyOpen !== "undefined") {
			initialState.modalIsOpen = !!this.props.modalIsInitialyOpen;
		} else if(typeof this.props.modalIsOpen === "undefined") {
			initialState.modalIsOpen = false;
		}

		// Return state
		return initialState;

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [
			'type', 'value', 'initialValue', 'disabled', 'readOnly', 'dropdownPosition',
			'onChange', 'onFocus', 'onBlur', 'isFocus', 'isInitialyFocus',
			'modalValue', 'modalInitialValue', 'modalAllowInputOptions', 'modalAllowValue', 'modalInitialAllowValue', 'modalAllowInputValue', 'modalInitialAllowInputValue', 'modalDenyInputOptions', 'modalDenyValue', 'modalInitialDenyValue', 'modalDenyInputValue', 'modalInitialDenyInputValue',
			'modalOnChange', 'modalOnOpen', 'modalOnClose', 'modalIsOpen', 'modalIsInitialyOpen', 'modalOnAllowInputFocus', 'modalOnAllowInputBlur', 'modalOnAllowInputChange', 'modalOnDenyInputFocus', 'modalOnDenyInputBlur', 'modalOnDenyInputChange',
			'className'
		]);
	},

	// Get interactive data
	_getInteractiveData: function() {

		// Data
		var interactiveData = {};

		// Get interactive data
		if(typeof this.state.value !== "undefined") {
			interactiveData.value = this.state.value;
		} else {
			interactiveData.value = this.props.value || "SELF";
		}
		if(typeof this.state.isFocus !== "undefined") {
			interactiveData.isFocus = this.state.isFocus;
		} else {
			interactiveData.isFocus = !!this.props.isFocus;
		}

		// Get modal interactive data
		if(typeof this.state.modalValue !== "undefined") {
			interactiveData.modalValue = this.state.modalValue;
		} else {
			interactiveData.modalValue = this.props.modalValue || { value: "CUSTOM", allow: [], deny: [] };
		}
		if(typeof this.state.modalAllowValue !== "undefined") {
			interactiveData.modalAllowValue = this.state.modalAllowValue;
		} else {
			interactiveData.modalAllowValue = this.props.modalAllowValue || "";
		}
		if(typeof this.state.modalAllowInputValue !== "undefined") {
			interactiveData.modalAllowInputValue = this.state.modalAllowInputValue;
		} else {
			interactiveData.modalAllowInputValue = this.props.modalAllowInputValue || "";
		}
		if(typeof this.state.modalDenyValue !== "undefined") {
			interactiveData.modalDenyValue = this.state.modalDenyValue;
		} else {
			interactiveData.modalDenyValue = this.props.modalDenyValue || "";
		}
		if(typeof this.state.modalDenyInputValue !== "undefined") {
			interactiveData.modalDenyInputValue = this.state.modalDenyInputValue;
		} else {
			interactiveData.modalDenyInputValue = this.props.modalDenyInputValue || "";
		}
		if(typeof this.state.modalIsOpen !== "undefined") {
			interactiveData.modalIsOpen = this.state.modalIsOpen;
		} else {
			interactiveData.modalIsOpen = !!this.props.modalIsOpen;
		}

		// Return interactive data
		return interactiveData;

	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.value;

		// Set state if modal will close
		if(this.props.modalIsOpen === true && nextProps.modalIsOpen === false && nextProps.modalValue === "undefined") {
			this.setState({
				modalValue: newModalValue
			});
		}

	},

	// Render
	render: function() {

		// Data
		var interactiveData = this._getInteractiveData();
		var value = interactiveData.value;
		var isFocus = interactiveData.isFocus;
		var type = this.props.type || 'white';
		var action = this.props.action;
		var componentAttributes = this._getComponentAttributes(this.props);
		var buttonClasses = classNames("form-control", "form-share", this.props.className);
		var disabled = !!this.props.disabled;
		var readOnly = !!this.props.readOnly;
		var dropdownPosition = this.props.dropdownPosition || 'belowLeft';
		var selectedOption = null;
		var iconClasses = null;
		var label = null;

		// Modal data
		var modalValue = interactiveData.modalValue;
		var modalAllowInputOptions = this.props.modalAllowInputOptions instanceof Array && this.props.modalAllowInputOptions.length > 0 ? this.props.modalAllowInputOptions : [];
		var modalAllowValue = interactiveData.modalAllowValue;
		var modalAllowInputValue = interactiveData.modalAllowInputValue;
		var modalDenyInputOptions = this.props.modalDenyInputOptions instanceof Array && this.props.modalDenyInputOptions.length > 0 ? this.props.modalDenyInputOptions : [];
		var modalDenyValue = interactiveData.modalDenyValue;
		var modalDenyInputValue = interactiveData.modalDenyInputValue;
		var modalIsOpen = interactiveData.modalIsOpen;

		// Check value
		if(value === "SELF") {
			selectedOption = "SELF";
			iconClasses = classNames("icn", { "icn-share-button-owner": !disabled, "icn-share-button-owner-light": disabled });
			label = "Uniquement moi";
		} else if(typeof value === "object" && value.value === "WORKSPACE" && value.permission === "READ") {
			selectedOption = "WORKSPACE_READ";
			iconClasses = classNames("icn", { "icn-share-button-workspace": !disabled, "icn-share-button-workspace-light": disabled });
			label = "Workspace - Lecture";
		} else if(typeof value === "object" && value.value === "WORKSPACE" && value.permission === "SPEAK") {
			selectedOption = "WORKSPACE_SPEAK";
			iconClasses = classNames("icn", { "icn-share-button-workspace": !disabled, "icn-share-button-workspace-light": disabled });
			label = "Workspace - Commentaire";
		} else if(typeof value === "object" && value.value === "WORKSPACE" && value.permission === "WRITE") {
			selectedOption = "WORKSPACE_WRITE";
			iconClasses = classNames("icn", { "icn-share-button-workspace": !disabled, "icn-share-button-workspace-light": disabled });
			label = "Workspace - Écriture";
		} else if(typeof value === "object" && value.value === "CUSTOM") {
			selectedOption = "CUSTOM";
			iconClasses = classNames("icn", { "icn-share-button-custom": !disabled, "icn-share-button-custom-light": disabled });
			label = "Personnalisé";
		}

		// Set modal
		var modal = (
			<Modal
				isOpen={modalIsOpen}
				onOpen={this._onModalOpen}
				onClose={this._onModalClose}
				light>

				{ modalIsOpen
					? (
						<div className="form-share-modal">

							<div className="header">
								<span className="title">Partage personnalisé</span>
								<p className="details">
									Vous pouvez autoriser ou refuser l'accès à vos collaborateurs,
									groupes et/ou à votre workspace.
								</p>
							</div>

							<div className="content">

								<div className="section section-allow">

									<span className="title">Partager avec :</span>

									<ul className="v-list section-list">

										{ modalValue.allow.length > 0
											? _.map(modalValue.allow, function(allowItem, allowItemIndex) {
												return(
													<li className="section-list-item" key={allowItemIndex}>
														<div className="t-group">
															<div className="t-cell marged">
																<Button type="blank" onClick={this._onModalAllowItemRemove.bind(this, allowItemIndex)}>
																	<span className="icn icn-remove"></span>
																</Button>
															</div>
															<div className="t-cell max-width">
																<Avatar className="contact-avatar" size="icn" href={null} hrefTitle={allowItem.target.name} imageSrc={allowItem.target.avatar} inline />
																<span>&nbsp;</span>
																<span>{allowItem.target.name}</span>
																<span>&nbsp;</span>
																<span className="light">•</span>
																<span>&nbsp;</span>
																<span className="light">{allowItem.target.type}</span>
															</div>
															<div className="t-cell">
																<ControlSelect
																	type="blank"
																	value={allowItem.permission}
																	options={[
																		{ value: "READ", label: "Lecture" },
																		{ value: "SPEAK", label: "Commentaire" },
																		{ value: "WRITE", label: "Écriture" }
																	]}
																	onChange={this._onModalAllowItemPermissionChange.bind(this, allowItemIndex)} />
															</div>
														</div>
													</li>
												);
											}.bind(this))
											: null
										}

									</ul>

									<div className="add-action">
										<ControlAutocomplete
											type="text"
											value={modalAllowValue}
											inputValue={modalAllowInputValue}
											options={modalAllowInputOptions}
											placeholder="Collaborateur, groupe, workspace..."
											matcher={true}
											highlighter={true}
											onInputChange={this._onModalAllowInputChange}
											onChange={this._onModalAllowSelect} />
									</div>

								</div>

								<div className="section section-deny">

									<span className="title">Refuser l'accès à :</span>

									<ul className="v-list section-list">

										{ modalValue.deny.length > 0
											? _.map(modalValue.deny, function(denyItem, denyItemIndex) {
												return(
													<li className="section-list-item" key={denyItemIndex}>
														<div className="t-group">
															<div className="t-cell marged">
																<Button type="blank" onClick={this._onModalDenyItemRemove.bind(this, denyItemIndex)}>
																	<span className="icn icn-remove"></span>
																</Button>
															</div>
															<div className="t-cell max-width">
																<Avatar className="contact-avatar" size="icn" href={null} hrefTitle={denyItem.name} imageSrc={denyItem.avatar} inline />
																<span>&nbsp;</span>
																<span>{denyItem.name}</span>
																<span>&nbsp;</span>
																<span className="light">•</span>
																<span>&nbsp;</span>
																<span className="light">{denyItem.type}</span>
															</div>
														</div>
													</li>
												);
											}.bind(this))
											: null
										}

									</ul>

									<div className="add-action">
										<ControlAutocomplete
											type="text"
											value={modalDenyValue}
											inputValue={modalDenyInputValue}
											options={modalDenyInputOptions}
											placeholder="Collaborateur"
											matcher={true}
											highlighter={true}
											onInputChange={this._onModalDenyInputChange}
											onChange={this._onModalDenySelect} />
									</div>

								</div>

							</div>

							<div className="footer">
								<ul className="h-list marged-list">
									<li><Button type="white" onClick={this._onModalClose}><span>Annuler</span></Button></li>
									<li><Button type="blue" onClick={this._onModalSave}><span>Confirmer</span></Button></li>
								</ul>
							</div>

						</div>
					)
					: null
				}
						
			</Modal>
		);

		// Return
		return (			
			<Button
				type={type}
				readOnly={readOnly}
				disabled={disabled}
				dropdown={!readOnly}
				className={buttonClasses}
				aria-label="Share button">

				<span className={iconClasses}></span>
				<span>&nbsp;&nbsp;</span>
				<span>{label}</span>

				<Dropdown
					position={dropdownPosition}
					helperPosition={dropdownPosition}
					className="form-share-dropdown"
					onShow={this._onFocus}
					onHide={this._onBlur}
					isShown={isFocus}>

					<ButtonList small>
						<ButtonListItem readonly className="btn-share-workspace" disabled>
							<span>Workspace</span>
						</ButtonListItem>
						<ButtonList small className="list-children">
							<ButtonListItem className={classNames({ selected: selectedOption === "WORKSPACE_READ" })} onClick={this._onChange.bind(this, "WORKSPACE_READ")}>
								<div className="sub-structure" />
								<span>Lecture</span>
							</ButtonListItem>
							<ButtonListItem className={classNames({ selected: selectedOption === "WORKSPACE_SPEAK" })} onClick={this._onChange.bind(this, "WORKSPACE_SPEAK")}>
								<div className="sub-structure" />
								<span>Commentaire</span>
							</ButtonListItem>
							<ButtonListItem className={classNames({ selected: selectedOption === "WORKSPACE_WRITE" })} onClick={this._onChange.bind(this, "WORKSPACE_WRITE")}>
								<div className="sub-structure" />
								<span>Écriture</span>
							</ButtonListItem>
						</ButtonList>
						<ButtonListItem className={classNames("btn-share-owner", { selected: selectedOption === "SELF" })} onClick={this._onChange.bind(this, "SELF")}>
							<span>Uniquement moi</span>
						</ButtonListItem>
						<ButtonListItem className={classNames("btn-share-custom", { selected: selectedOption === "CUSTOM" })}>
							<span>Personnalisé...</span>
							{modal}
						</ButtonListItem>
					</ButtonList>

				</Dropdown>

			</Button>
		);

	},

	// On change function
	_onChange: function(value, e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var isFocus = interactiveData.isFocus;
		var modalIsOpen = interactiveData.modalIsOpen;
		var newState = {};

		// Check value
		if(value === "SELF") {
			newState.value = "SELF";
		} else if(value === "WORKSPACE_READ") {
			newState.value = { value: "WORKSPACE", permission: "READ" };
		} else if(value === "WORKSPACE_SPEAK") {
			newState.value = { value: "WORKSPACE", permission: "SPEAK" };
		} else if(value === "WORKSPACE_WRITE") {
			newState.value = { value: "WORKSPACE", permission: "WRITE" };
		} else {
			newState.value = _.clone(value);
		}

		// If uncontrolled data
		if(typeof this.props.value === "undefined") {

			// Set state
			this.setState(newState);

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(newState.value, e);
		}

		// Close modal and blur
		if(modalIsOpen) this._onModalClose(e);
		if(isFocus) this._onBlur(e, true);

	},

	// On modal save
	_onModalSave: function(e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var modalValue = interactiveData.modalValue;
		var newValue = modalValue;

		// Change
		this._onChange(modalValue, e);

	},

	// On modal open
	_onModalOpen: function(e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var value = interactiveData.value;
		var modalIsOpen = interactiveData.modalIsOpen;
		var newState = {};

		// Check if already open
		if(modalIsOpen) {
			return;
		}

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Get new modal value
			if(typeof value === "object" && value !== null && value.value === "CUSTOM") {
				newState.modalValue = deepcopy(value);
			} else {
				newState.modalValue = deepcopy(this.props.modalInitialValue) || { value: "CUSTOM", allow: [], deny: [] }
			}

		}

		// If uncontrolled data
		if(typeof this.props.modalIsOpen === "undefined") {

			// Set modal open state to true
			newState.modalIsOpen = true;

		}

		// Set state
		this.setState(newState);

		// Modal open
		if(_.isFunction(this.props.onModalOpen)) {
			this.props.onModalOpen(e);
		}

	},

	// On modal close
	_onModalClose: function(e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var modalIsOpen = interactiveData.modalIsOpen;

		// Check if already close
		if(!modalIsOpen) {
			return;
		}

		// If controlled data
		if(typeof this.props.modalIsOpen === "undefined") {

			// Set modal open state to false
			this.setState({
				modalIsOpen: false
			});

		}

		// Modal close
		if(_.isFunction(this.props.onModalClose)) {
			this.props.onModalClose(e);
		}

	},

	// On focus function
	_onFocus: function(e) {

		// Check if read only
		if(this.props.readOnly || this.props.disabled) {
			return;
		}

		// If controlled data
		if(typeof this.props.isFocus !== "undefined") {

			// Check if already focus
			if(this.props.isFocus) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already focus
			if(this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: true
			});

		}

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

	},

	// On blur function
	_onBlur: function(e, force) {

		// Data
		var interactiveData = this._getInteractiveData();
		var isFocus = interactiveData.isFocus;
		var modalIsOpen = interactiveData.modalIsOpen;

		// Return if modal is open or is already blur
		if((!force && modalIsOpen) || !isFocus) {
			return;
		}

		// If uncontrolled data
		if(typeof this.props.isFocus === "undefined") {

			// Set state
			this.setState({
				isFocus: false
			});

		}

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

	},

	// On modal allow item permission change function
	_onModalAllowItemPermissionChange: function(index, value, e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.modalValue;

		// Change item permission
		if(newModalValue.allow instanceof Array && newModalValue.allow[index] !== "undefined") {
			newModalValue.allow[index].permission = value;
		}

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Set state
			this.setState({
				modalValue: newModalValue
			});

		}

		// Modal change
		if(_.isFunction(this.props.modalOnChange)) {
			this.props.modalOnChange(newModalValue);
		}

	},

	// On modal allow item remove function
	_onModalAllowItemRemove: function(index, e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.modalValue;

		// Change item permission
		if(newModalValue.allow instanceof Array && newModalValue.allow[index] !== "undefined") {
			newModalValue.allow.splice(index, 1);
		}

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Set state
			this.setState({
				modalValue: newModalValue
			});

		}

		// Modal change
		if(_.isFunction(this.props.modalOnChange)) {
			this.props.modalOnChange(newModalValue);
		}

	},

	// On modal allow input change
	_onModalAllowInputChange: function(value) {

		// If uncontrolled data
		if(typeof this.props.modalAllowInputValue === "undefined") {

			// Set state
			this.setState({
				modalAllowInputValue: value
			});

		}

		// Modal allow input change
		if(_.isFunction(this.props.modalOnAllowInputChange)) {
			this.props.modalOnAllowInputChange(value);
		}

	},

	// On modal allow select
	_onModalAllowSelect: function(value) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.modalValue;

		// Check if allow/deny are array
		if(!(newModalValue.allow instanceof Array)) {
			newModalValue.allow = [];
		}
		if(!(newModalValue.deny instanceof Array)) {
			newModalValue.deny = [];
		}

		// Check if not already defined in deny
		var denyItem = _.find(newModalValue.deny, function(obj) { return obj.pid === value.pid; });
		var denyItemIndex = newModalValue.deny.indexOf(denyItem);
		if(denyItemIndex !== -1) {
			newModalValue.deny.splice(denyItemIndex, 1);
		}

		// Check if not already defined in allow
		var allowItem = _.find(newModalValue.allow, function(obj) { return obj.target.pid === value.pid; });
		var allowItemIndex = newModalValue.allow.indexOf(allowItem);
		if(allowItemIndex !== -1) {
			newModalValue.allow.splice(allowItemIndex, 1);
		}

		// Add new value
		newModalValue.allow.push({ target: value, permission: "READ" });

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Set state
			this.setState({
				modalValue: newModalValue
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalAllowValue === "undefined") {

			// Set state
			this.setState({
				modalAllowValue: ''
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalAllowInputValue === "undefined") {

			// Set state
			this.setState({
				modalAllowInputValue: ''
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalAllowInputOptions === "undefined") {

			// Set state
			this.setState({
				modalAllowInputOptions: []
			});

		}

		// Modal change
		if(_.isFunction(this.props.modalOnChange)) {
			this.props.modalOnChange(newModalValue);
		}

		// Modal change
		if(_.isFunction(this.props.modalOnAllowChange)) {
			this.props.modalOnAllowChange('');
		}

		// Modal change
		if(_.isFunction(this.props.modalOnAllowInputChange)) {
			this.props.modalOnAllowInputChange('');
		}

	},

	// On modal deny item remove function
	_onModalDenyItemRemove: function(index, e) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.modalValue;

		// Change item permission
		if(newModalValue.deny instanceof Array && newModalValue.deny[index] !== "undefined") {
			newModalValue.deny.splice(index, 1);
		}

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Set state
			this.setState({
				modalValue: newModalValue
			});

		}

		// Modal change
		if(_.isFunction(this.props.modalOnChange)) {
			this.props.modalOnChange(newModalValue);
		}

	},

	// On modal deny input change
	_onModalDenyInputChange: function(value) {

		// If uncontrolled data
		if(typeof this.props.modalDenyInputValue === "undefined") {

			// Set state
			this.setState({
				modalDenyInputValue: value
			});

		}

		// Modal deny input change
		if(_.isFunction(this.props.modalOnDenyInputChange)) {
			this.props.modalOnDenyInputChange(value);
		}

	},

	// On modal deny select
	_onModalDenySelect: function(value) {

		// Data
		var interactiveData = this._getInteractiveData();
		var newModalValue = interactiveData.modalValue;

		// Check if allow/deny are array
		if(!(newModalValue.allow instanceof Array)) {
			newModalValue.allow = [];
		}
		if(!(newModalValue.deny instanceof Array)) {
			newModalValue.deny = [];
		}

		// Check if not already defined in deny
		var denyItem = _.find(newModalValue.deny, function(obj) { return obj.pid === value.pid; });
		var denyItemIndex = newModalValue.deny.indexOf(denyItem);
		if(denyItemIndex !== -1) {
			newModalValue.deny.splice(denyItemIndex, 1);
		}

		// Check if not already defined in allow
		var allowItem = _.find(newModalValue.allow, function(obj) { return obj.target.pid === value.pid; });
		var allowItemIndex = newModalValue.allow.indexOf(allowItem);
		if(allowItemIndex !== -1) {
			newModalValue.allow.splice(allowItemIndex, 1);
		}

		// Add new value
		newModalValue.deny.push(value);

		// If uncontrolled data
		if(typeof this.props.modalValue === "undefined") {

			// Set state
			this.setState({
				modalValue: newModalValue
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalDenyValue === "undefined") {

			// Set state
			this.setState({
				modalDenyValue: ''
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalDenyInputValue === "undefined") {

			// Set state
			this.setState({
				modalDenyInputValue: ''
			});

		}

		// If uncontrolled data
		if(typeof this.props.modalDenyInputOptions === "undefined") {

			// Set state
			this.setState({
				modalDenyInputOptions: []
			});

		}

		// Modal change
		if(_.isFunction(this.props.modalOnChange)) {
			this.props.modalOnChange(newModalValue);
		}

		// Modal change
		if(_.isFunction(this.props.modalOnDenyChange)) {
			this.props.modalOnDenyChange('');
		}

		// Modal change
		if(_.isFunction(this.props.modalOnDenyInputChange)) {
			this.props.modalOnDenyInputChange('');
		}

	}
	
});


// Exports component
module.exports = FormShareButton;