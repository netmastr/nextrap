var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');
var moment = require('moment');


// Get components
var Button = require('../elements/button.react');
var Dropdown = require('../contextual/dropdown.react');
var Select = require('./control-select.react');



// Check moment props type : date
var _ReactPropTypesDate = function(props, propName, componentName) {

	// Check if instance of Date / moment or is null
	if(!(moment.isDate(props[propName]) || moment.isMoment(props[propName]) || typeof props[propName] === "undefined" || props[propName] === null)) {
		return new Error(propName + ' must be an instance of moment or a Date.');
	}

};



// Set component : FormDateInput
var FormDateInput = React.createClass({

	// Prop types
	propTypes: {

		// Important
		datetime: React.PropTypes.bool,
		format: React.PropTypes.string,
		type: React.PropTypes.string,
		placeholder: React.PropTypes.oneOfType([ React.PropTypes.string, _ReactPropTypesDate ]),
		value: _ReactPropTypesDate,
		initialValue: _ReactPropTypesDate,
		compact: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		isBlurOnChange: React.PropTypes.bool,
		dropdownProps: React.PropTypes.object,

		// Date picker
		currentDate: _ReactPropTypesDate,
		displayedMonth: _ReactPropTypesDate,
		initialDisplayedMonth: _ReactPropTypesDate,
		minDate: _ReactPropTypesDate,
		maxDate: _ReactPropTypesDate,

		// Plus
		className: React.PropTypes.string,
		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func,
		isFocus: React.PropTypes.bool,
		isInitialyFocus: React.PropTypes.bool

	},

	// Default props
	getDefaultProps: function() {
		return {

			// Important
			format: "YYYY, MMM DD",
			isBlurOnChange: true

		};
	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};
		var formatedProps = this._formateProps(this.props);

		// Set state
		if(typeof this.props.value === "undefined" && typeof this.props.initialValue !== "undefined") {
			initialState.value = formatedProps.initialValue;
		} else if(typeof this.props.value === "undefined") {
			initialState.value = null;
		}
		if(typeof this.props.displayedMonth === "undefined" && typeof this.props.initialDisplayedMonth !== "undefined") {
			initialState.displayedMonth = formatedProps.initialDisplayedMonth;
		} else if(typeof this.props.displayedMonth === "undefined") {
			initialState.displayedMonth = null;
		}
		if(typeof this.props.isFocus === "undefined" && typeof this.props.isInitialyFocus !== "undefined") {
			initialState.isFocus = !!this.props.isInitialyFocus;
		} else if(typeof this.props.isFocus === "undefined") {
			initialState.isFocus = false;
		}

		// Formate state
		if(typeof initialState.displayedMonth !== "undefined" && initialState.displayedMonth === null) {

			// If value is a controlled field
			if(typeof initialState.value === "undefined") {
				if(formatedProps.value !== null)
					initialState.displayedMonth = moment(formatedProps.value).startOf('month').startOf("day");
				if(formatedProps.value === null)
					initialState.displayedMonth = moment().startOf('month').startOf("day");
			}

			// If value is an uncontrolled field
			else if(typeof initialState.value !== "undefined") {
				if(initialState.value !== null)
					initialState.displayedMonth = moment(initialState.value).startOf('month').startOf("day");
				if(initialState.value === null)
					initialState.displayedMonth = moment().startOf('month').startOf("day");
			}

		}

		// Return state
		return initialState;

	},

	// Formate props
	_formateProps: function(props) {

		// Data
		var formatedProps = _.clone(props);

		// Formate props
		if(typeof formatedProps.value !== "undefined" && formatedProps.value !== null)
			formatedProps.value = formatedProps.datetime ? moment(formatedProps.value) : moment(formatedProps.value).startOf('day');
		if(typeof formatedProps.initialValue !== "undefined" && formatedProps.initialValue !== null)
			formatedProps.initialValue = formatedProps.datetime ? moment(formatedProps.initialValue) : mmoment(formatedProps.initialValue).startOf('day');
		if(typeof formatedProps.displayedMonth !== "undefined" && formatedProps.displayedMonth !== null)
			formatedProps.displayedMonth = formatedProps.displayedMonth ? moment(formatedProps.displayedMonth).startOf('month').startOf('day') : null;
		if(typeof formatedProps.initialDisplayedMonth !== "undefined" && formatedProps.initialDisplayedMonth !== null)
			formatedProps.initialDisplayedMonth = moment(formatedProps.initialDisplayedMonth).startOf('month').startOf('day');

		// Return
		return formatedProps;

	},

	// Render
	render: function() {

		// Get formated props
		var formatedProps = this._formateProps(this.props);

		// Get (in)controlled data
		if(typeof this.state.value !== "undefined") {
			var value = this.state.value;
		} else {
			var value = formatedProps.value || null;
		}
		if(typeof this.state.displayedMonth !== "undefined") {
			var displayedMonth = this.state.displayedMonth;
		} else {
			var displayedMonth = formatedProps.displayedMonth || null;
		}
		if(typeof this.state.isFocus !== "undefined") {
			var isFocus = this.state.isFocus;
		} else {
			var isFocus = !!this.props.isFocus;
		}

		// Formate
		if(displayedMonth === null && value !== null) {
			displayedMonth = moment(value).startOf('month').startOf("day");
		} else if(displayedMonth === null && value === null) {
			displayedMonth = moment().startOf('month').startOf("day");
		}

		// Datas
		var valueDate = value ? moment(value).startOf('day') : null;
		var currentDate = this.props.currentDate ? moment(this.props.currentDate).startOf("day") : null;
		var isDatetime = !!this.props.datetime;
		var type = this.props.type || "white";
		var placeholder = this.props.placeholder || null;
		var compact = !!this.props.compact;
		var readOnly = !!this.props.readOnly;
		var format = this.props.format;
		var classes = classNames("form-control", "form-date", { 'read-only': readOnly, 'compact': compact }, this.props.className);
		var buttonClasses = classNames({ placeholder: value === null });
		var dropdownProps = dropdownProps || {};

		// Check placeholder
		if(moment.isDate(placeholder) || moment.isMoment(placeholder)) {
			placeholder = moment(placeholder).format(format);
		}

		// Datepicker datas
		var weekdays = _.clone(moment.localeData()._weekdaysMin);
			weekdays.push(weekdays.shift());
		var calendarDays = [];
		var firstDay = parseInt(displayedMonth.format('e'));
		var lastDate = moment(displayedMonth).add(1, 'month').subtract(1, 'day');
		var lastDay = parseInt(lastDate.format('e'));
		var currentMonthDaysCount = parseInt(lastDate.format('D'));
		var prevMonthDaysCount = parseInt(moment(displayedMonth).subtract(1, 'day').format('D'));
		var prevMonth = moment(displayedMonth).subtract(1, 'month');
		var nextMonth = moment(displayedMonth).add(1, 'month');
		var minDate = this.props.minDate ? moment(this.props.minDate).startOf('day') : null;
		var maxDate = this.props.maxDate ? moment(this.props.maxDate).startOf('day') : null;
		var disabledMonthPrev = false;
		var disabledMonthNext = false;

		// Populate calendar
		for(var i = 0; i < 42; i++) {

			// Datas
			var dateIndex = (i - firstDay);
			var date = {};

			// If in prev month
			if(dateIndex < 0) {

				// Set date
				date = {
					date: (prevMonthDaysCount + dateIndex + 1),
					month: "prev"
				}

				// Get active
				if(valueDate && moment(prevMonth).date(date.date).diff(valueDate, 'days') === 0) {
					date.active = true;
				}

				// Get current
				if(currentDate && moment(prevMonth).date(date.date).diff(currentDate, 'days') === 0) {
					date.current = true;
				}

				// Get min date
				if(minDate !== null && moment(prevMonth).date(date.date).diff(minDate, 'days') < 0) {
					date.disabled = true;
					disabledMonthPrev = true;
				}

				// Get max date
				if(maxDate !== null && moment(prevMonth).date(date.date).diff(maxDate, 'days') > 0) {
					date.disabled = true;
					disabledMonthNext = true;
				}

				// Push to calendar days
				calendarDays.push(date);

			}

			// If in next month
			else if(dateIndex >= currentMonthDaysCount) {

				// Set date
				date = {
					date: ((dateIndex - currentMonthDaysCount) + 1),
					month: "next"
				}

				// Get active
				if(valueDate && moment(nextMonth).date(date.date).diff(valueDate, 'days') === 0) {
					date.active = true;
				}

				// Get current
				if(currentDate && moment(nextMonth).date(date.date).diff(currentDate, 'days') === 0) {
					date.current = true;
				}

				// Get min date
				if(minDate !== null && moment(nextMonth).date(date.date).diff(minDate, 'days') < 0) {
					date.disabled = true;
					disabledMonthPrev = true;
				}

				// Get max date
				if(maxDate !== null && moment(nextMonth).date(date.date).diff(maxDate, 'days') > 0) {
					date.disabled = true;
					disabledMonthNext = true;
				}

				// Push to calendar days
				calendarDays.push(date);

				// Check if finished
				if(i % 7 <= lastDay) {
					break;
				}

			}

			// If in display month
			else {

				// Set date
				date = {
					date: (dateIndex + 1),
					month: "display"
				}

				// Get active
				if(valueDate && moment(displayedMonth).date(date.date).diff(valueDate, 'days') === 0) {
					date.active = true;
				}

				// Get current
				if(currentDate && moment(displayedMonth).date(date.date).diff(currentDate, 'days') === 0) {
					date.current = true;
				}

				// Get min date
				if(minDate !== null && moment(displayedMonth).date(date.date).diff(minDate, 'days') < 0) {
					date.disabled = true;
					disabledMonthPrev = true;
				}

				// Get max date
				if(maxDate !== null && moment(displayedMonth).date(date.date).diff(maxDate, 'days') > 0) {
					date.disabled = true;
					disabledMonthNext = true;
				}

				// Push to calendar days
				calendarDays.push(date);

			}

		}
			
		// Return
		return(
			<div className={classes}>

				<Button
					type={type}
					disabled={readOnly}
					dropdown
					className={buttonClasses}
					ref="button">

					<span>
						{(function() {

							// If value
							if(value) {
								return value.format(format);
							} 

							// If placeholder not empty
							else if(placeholder) {
								return placeholder;
							}

						})()}
					</span>

					<Dropdown
						{...dropdownProps}
						helperPosition={dropdownProps.helperPosition || 'belowLeft'}
						position={dropdownProps.position || 'belowLeft'}
						className={classNames("form-date-dropdown", dropdownProps.className)}
						onShow={this._onFocus}
						onHide={this._onBlur}
						isShown={isFocus}
					>
						
						<div className="date-picker">

							<div className="date-picker-header">
								<div className="date-picker-prev">
									<Button type="icon" disabled={disabledMonthPrev} className="btn-prev" onClick={this._onMonthPrevClick.bind(this, disabledMonthPrev)}></Button>
								</div>
								<div className="date-picker-title">
									<span className="date-picker-month">{displayedMonth.format('MMMM')}</span>
									<span className="date-picker-year">{displayedMonth.format('YYYY')}</span>
								</div>
								<div className="date-picker-next">
									<Button type="icon" disabled={disabledMonthNext} className="btn-next" onClick={this._onMonthNextClick.bind(this, disabledMonthNext)}></Button>
								</div>
							</div>

							<div className="date-picker-subheader">
								<div className="date-picker-days">
									{weekdays.map(function(dayItem) {
										return (
											<div className="date-picker-day" key={dayItem}><span>{dayItem}</span></div>
										);
									}.bind(this))}
								</div>
							</div>

							<div className="date-picker-content">
								<div className="date-picker-calendar">

									<div className="date-picker-calendar-line">
										{calendarDays.slice(0,7).map(function(dayItem) {

											// Datas
											var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
											var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

											// Return
											return (
												<div className={classes} key={dayItem.date}>
													<span onClick={onClick}>{dayItem.date}</span>
												</div>
											);

										}.bind(this))}
									</div>

									<div className="date-picker-calendar-line">
										{calendarDays.slice(7,14).map(function(dayItem) {

											// Datas
											var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
											var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

											// Return
											return (
												<div className={classes} key={dayItem.date}>
													<span onClick={onClick}>{dayItem.date}</span>
												</div>
											);

										}.bind(this))}
									</div>

									<div className="date-picker-calendar-line">
										{calendarDays.slice(14,21).map(function(dayItem) {

											// Datas
											var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
											var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

											// Return
											return (
												<div className={classes} key={dayItem.date}>
													<span onClick={onClick}>{dayItem.date}</span>
												</div>
											);

										}.bind(this))}
									</div>

									<div className="date-picker-calendar-line">
										{calendarDays.slice(21,28).map(function(dayItem) {

											// Datas
											var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
											var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

											// Return
											return (
												<div className={classes} key={dayItem.date}>
													<span onClick={onClick}>{dayItem.date}</span>
												</div>
											);

										}.bind(this))}
									</div>

									{calendarDays.length >= 35 ? (

										<div className="date-picker-calendar-line">
											{calendarDays.slice(28,35).map(function(dayItem) {

												// Datas
												var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
												var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

												// Return
												return (
													<div className={classes} key={dayItem.date}>
														<span onClick={onClick}>{dayItem.date}</span>
													</div>
												);

											}.bind(this))}
										</div>

									) : false }

									{calendarDays.length >= 42 ? (

										<div className="date-picker-calendar-line">
											{calendarDays.slice(35,42).map(function(dayItem) {

												// Datas
												var classes = classNames("date-picker-date", dayItem.month + "-month", { active: dayItem.active, disabled: dayItem.disabled, current: dayItem.current });
												var onClick = this._onChange.bind(this, dayItem.date, dayItem.month, dayItem.disabled);

												// Return
												return (
													<div className={classes} key={dayItem.date}>
														<span onClick={onClick}>{dayItem.date}</span>
													</div>
												);

											}.bind(this))}
										</div>

									) : false }

								</div>
							</div>

						</div>

						{ isDatetime
							? (
								<div className="time-picker">
									<div className='t-group'>

										<div className='t-cell keyword'><span>à</span></div>
										<div className='t-cell select'>
											<Select
												type='white'
												value={value.format('HH')}
												options={[
													{ value: '00' },
													{ value: '01' },
													{ value: '02' },
													{ value: '03' },
													{ value: '04' },
													{ value: '05' },
													{ value: '06' },
													{ value: '07' },
													{ value: '08' },
													{ value: '09' },
													{ value: '10' },
													{ value: '11' },
													{ value: '12' },
													{ value: '13' },
													{ value: '14' },
													{ value: '15' },
													{ value: '16' },
													{ value: '17' },
													{ value: '18' },
													{ value: '19' },
													{ value: '20' },
													{ value: '21' },
													{ value: '22' },
													{ value: '23' }
												]}
												onChange={this._onTimeChange.bind(this, 'h')} />
										</div>
										<div className='t-cell keyword'><span>h</span></div>
										<div className='t-cell select'>
											<Select
												type='white'
												value={value.format('mm')}
												options={[
													{ value: '00' },
													{ value: '05' },
													{ value: '10' },
													{ value: '15' },
													{ value: '20' },
													{ value: '25' },
													{ value: '30' },
													{ value: '35' },
													{ value: '40' },
													{ value: '45' },
													{ value: '50' },
													{ value: '55' }
												]}
												onChange={this._onTimeChange.bind(this, 'm')} />
										</div>
										<div className='t-cell keyword'><span>min</span></div>

									</div>
								</div>
							)
							: null
						}

					</Dropdown>

				</Button>

			</div>
		);

	},

	// On change
	_onChange: function(date, month, disabled, e) {

		// Check if disabled
		if(disabled) {
			return;
		}

		// Get formated props
		var formatedProps = this._formateProps(this.props);

		// Get (in)controlled data
		if(typeof this.state.value !== "undefined") {
			var value = this.state.value;
		} else {
			var value = formatedProps.value || null;
		}
		if(typeof this.state.displayedMonth !== "undefined") {
			var displayedMonth = this.state.displayedMonth;
		} else {
			var displayedMonth = formatedProps.displayedMonth || null;
		}

		// Formate
		if(displayedMonth === null && value !== null) {
			displayedMonth = moment(value).startOf('month').startOf("day");
		} else if(displayedMonth === null && value === null) {
			displayedMonth = moment().startOf('month').startOf("day");
		}

		// Data
		var newState = {};

		// Check if prev month
		if(month === "prev") {

			// Set new selected date
			newState.value = moment()
				.year(moment(displayedMonth).subtract(1, "month").year())
				.month(moment(displayedMonth).subtract(1, "month").month())
				.date(date)
				.startOf("day");

			// Set displayed month
			newState.displayedMonth = moment(displayedMonth).subtract(1, "month");

		}

		// Check if next month
		else if(month === "next") {

			// Set new selected date
			newState.value = moment()
				.year(moment(displayedMonth).add(1, "month").year())
				.month(moment(displayedMonth).add(1, "month").month())
				.date(date)
				.startOf("day");

			// Set displayed month
			newState.displayedMonth = moment(displayedMonth).add(1, "month");

		}

		// Check if display month
		else {

			// Set new selected date
			newState.value = moment()
				.year(moment(displayedMonth).year())
				.month(moment(displayedMonth).month())
				.date(date)
				.startOf("day");

		}

		// If datetime
		if(this.props.datetime) {
			newState.value = newState.value
				.hour(parseInt(value.format('HH')))
				.minute(parseInt(value.format('mm')));
		}

		// If uncontrolled data
		if(typeof this.props.value === "undefined") {

			// Set state
			this.setState(newState);

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(newState.value, e);
		}

		// If blur on change
		if(this.props.isBlurOnChange) {
			this._onBlur(e);
		}

	},

	// On time change
	_onTimeChange: function(type, value, e) {

		// Get formated props
		var formatedProps = this._formateProps(this.props);

		// Get (in)controlled data
		if(typeof this.state.value !== "undefined") {
			var datetime = this.state.value;
		} else {
			var datetime = formatedProps.value || null;
		}

		// Data
		var newState = {};

		// Set new selected date
		if(type === 'h') {
			newState.value = moment(datetime).hour(parseInt(value));
		} else if(type === 'm') {
			newState.value = moment(datetime).minute(parseInt(value));
		}

		// If uncontrolled data
		if(typeof this.props.value === "undefined") {

			// Set state
			this.setState(newState);

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(newState.value, e);
		}

		// If blur on change
		if(this.props.isBlurOnChange) {
			this._onBlur(e);
		}

	},

	// On month prev function
	_onMonthPrevClick: function(disabled) {

		// Check if disabled
		if(!disabled) {

			// Set state
			this.setState({
				displayedMonth : moment(this.state.displayedMonth).subtract(1, "month")
			});

		}

	},

	// On month next function
	_onMonthNextClick: function(disabled) {

		// Check if disabled
		if(!disabled) {

			// Set state
			this.setState({
				displayedMonth : moment(this.state.displayedMonth).add(1, "month")
			});

		}

	},

	// On focus function
	_onFocus: function(e) {

		// Check if not disabled
		if(this.props.readOnly) {
			return;
		}

		// If controlled data
		if(typeof this.props.isFocus !== "undefined") {

			// Check if already focus
			if(this.props.isFocus) {
				return;
			}

		}

		// If incontrolled data
		else {

			// Check if already focus
			if(this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: true
			});

		}

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

	},

	// On blur function
	_onBlur: function(e) {

		// If controlled data
		if(typeof this.props.isFocus !== "undefined") {

			// Check if already hide
			if(!this.props.isFocus) {
				return;
			}

		}

		// If incontrolled data
		else {

			// Check if already hide
			if(!this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: false
			});

		}

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

	}

});


// Exports component
module.exports = FormDateInput;