var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Get components
var Button = require('../elements/button.react');


// Set component : FormSelect
var FormSelect = React.createClass({

	// Prop types
	propTypes: {

		// Important
		options: React.PropTypes.array.isRequired,
		placeholder: React.PropTypes.string,
		value: React.PropTypes.any,
		initialValue: React.PropTypes.string,
		disabled: React.PropTypes.bool,
		type: React.PropTypes.string,
		small: React.PropTypes.bool,
		compact: React.PropTypes.bool,
		loading: React.PropTypes.bool,

		// Plus
		className: React.PropTypes.string,
		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.value === "undefined" && typeof this.props.initialValue !== "undefined") {
			initialState.value = this.props.initialValue || "";
		} else if(typeof this.props.value === "undefined") {
			initialState.value = "";
		}

		// Return state
		return initialState;

	},

	// Render
	render: function() {

		// Get (un)controlled data
		if(typeof this.state.value !== "undefined") {
			var value = this.state.value;
		} else {
			var value = this.props.value || "";
		}

		// Set data
		var placeholder = this.props.placeholder || "";
		var type = this.props.type || "white";
		var isCompact = !!this.props.compact;
		var isSmall = !!this.props.small;
		var isLoading = !!this.props.loading;
		var isDisabled = !!this.props.disabled;
		var selectIsDisabled = isLoading || isDisabled;
		var options = _.clone(this.props.options);
		var selectedOption = _.find(options, function(obj) { return obj.value === value || (!!obj.value === false && !value); });
		var selectedOptionIndex = _.findIndex(options, function(obj) { return obj.value === value || (!!obj.value === false && !value); });
		var placeholderOption = _.find(options, function(obj) { return obj.value === placeholder || (!!obj.value === false && !placeholder); });
		var classes = classNames("form-control", "form-select", { disabled: isDisabled, compact: isCompact, loading: isLoading }, this.props.className);
		var buttonClasses = classNames({ placeholder: placeholderOption === selectedOption });

		// Set button label
		if(selectedOption) {
			var buttonLabel = typeof selectedOption.label !== 'undefined' ? selectedOption.label : selectedOption.value;
		} else if(placeholderOption) {
			var buttonLabel = typeof placeholderOption.label !== 'undefined' ? placeholderOption.label : placeholderOption.value;
		} else {
			var buttonLabel = 'Select';
		}

		// Return
		return(
			<div className={classes}>

				<select
					value={selectedOptionIndex}
					onChange={this._onChange}
					onFocus={this._onFocus}
					onBlur={this._onBlur}
					disabled={selectIsDisabled}
					ref="select">

					{ options.map(function(optionItem, optionIndex) {
						return (
							<option value={optionIndex} key={optionIndex} disabled={placeholderOption === optionItem}>{typeof optionItem.label !== 'undefined' ? optionItem.label : optionItem.value}</option>
						);
					}) }

				</select>

				<Button
					type={type}
					small={isSmall}
					disabled={isDisabled}
					loading={isLoading}
					dropdown
					className={buttonClasses}
					ref="button">
					<span>{buttonLabel}</span>
				</Button>
				
			</div>
		);

	},

	// On change
	_onChange: function(e) {

		// Check if disabled
		if(this.props.disabled) {
			return;
		}

		// Datas
		var value = e.currentTarget.value ? this.props.options[e.currentTarget.value].value : "";

		// If uncontrolled data
		if(typeof this.props.value === "undefined") {

			// Set state
			this.setState({
				value: value
			});

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(value, e);
		}

	},

	// On focus function
	_onFocus: function(e) {

		// If controlled data
		if(typeof this.props.isFocus !== "undefined") {

			// Check if already focus
			if(this.props.isFocus) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already focus
			if(this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: true
			});

		}

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

	},

	// On blur function
	_onBlur: function(e) {

		// If controlled data
		if(typeof this.props.isFocus !== "undefined") {

			// Check if already blur
			if(!this.props.isFocus) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already blur
			if(!this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: false
			});

		}

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

	}

});


// Exports component
module.exports = FormSelect;