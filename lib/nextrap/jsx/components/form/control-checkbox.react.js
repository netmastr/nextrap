var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : FormCheckbox
var FormCheckbox = React.createClass({

	// Prop types
	propTypes: {

		// Important
		name: React.PropTypes.string,
		label: React.PropTypes.string,
		isChecked: React.PropTypes.bool,
		isInitialyChecked: React.PropTypes.bool,

		// Plus
		onChange: React.PropTypes.func

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.isChecked === "undefined" && typeof this.props.isInitialyChecked !== "undefined") {
			initialState.isChecked = !!this.props.isInitialyChecked;
		} else if(typeof this.props.isChecked === "undefined") {
			initialState.isChecked = false;
		}

		// Return state
		return initialState;

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'type', 'name', 'label', 'isChecked', 'isInitialyChecked', 'onChange', 'className' ]);
	},

	// Render
	render: function() {

		// Get (in)controlled data
		if(typeof this.state.isChecked !== "undefined") {
			var isChecked = this.state.isChecked;
		} else {
			var isChecked = !!this.props.isChecked;
		}

		// Datas
		var name = this.props.name || null;
		var label = this.props.label || null;
		var classes = classNames("form-control", "form-checkbox", this.props.className, { checked: isChecked });
		var componentAttributes = this._getComponentAttributes(this.props);
			
		// Return
		return(
			<div {...componentAttributes} className={classes} onClick={this._onClick}>
				<span className="checkbox-icon"></span>
				{ label
					? <span className="checkbox-label">{label}</span>
					: false
				}
				<input type="hidden" name={name} value={isChecked} />
			</div>
		);

	},

	// On change
	_onClick: function(e) {

		// If incontrolled data
		if(typeof this.props.isChecked === "undefined") {

			// Set data
			var isChecked = !this.state.isChecked;

			// Set state
			this.setState({
				isChecked: isChecked
			});

		}

		// If controlled data
		else {

			// Set data
			var isChecked = !this.props.isChecked;

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(isChecked, e);
		}
		
	}

});


// Exports component
module.exports = FormCheckbox;