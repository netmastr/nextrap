var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Get components
var Button = require('../elements/button.react');
var Dropdown = require('../contextual/dropdown.react');
var ControlText = require('./control-text.react');


/////////////////////////////////////
//
// TO REDESIGN : EVENTS, ...
//
/////////////////////////////////////


// Set component : FormFileInput
var FormFileInput = React.createClass({

	// Prop types
	propTypes: {

		// Important
		action: React.PropTypes.string,
		placeholder: React.PropTypes.string,
		value: React.PropTypes.string,
		valueType: React.PropTypes.string,

		// File
		fileName: React.PropTypes.string,
		fileInputAction: React.PropTypes.string,
		fileInputDetails: React.PropTypes.string,

		// Url
		urlName: React.PropTypes.string,
		urlInputAction: React.PropTypes.string,
		urlInputDetails: React.PropTypes.string,
		urlInputValue: React.PropTypes.string,
		urlInputPlaceholder: React.PropTypes.string,

		// Plus
		className: React.PropTypes.string,
		onFileAdd: React.PropTypes.func,
		onUrlAdd: React.PropTypes.func,
		onUrlInputChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func

	},

	// Default props
	getDefaultProps: function() {
		return {

			// Important
			action: "Add a file",
			placeholder: "No file has been added",

			// File
			fileInputAction: "Upload a file",
			fileInputDetails: "Add a file from your computer",

			// Url
			urlInputAction: "Provide an url",
			urlInputDetails: "Add a file from the web",
			urlInputPlaceholder: "http://mywebsite.com/my_photo.jpg",

		};
	},

	// Formate mutable props
	formateMutableProps: function(props, forceValue) {

		// Datas
		var formatedProps = {};

		// Formate props
		if(typeof props.value !== "undefined" || forceValue)
			formatedProps.value = props.value || "";
		if(typeof props.valueType !== "undefined" || forceValue)
			formatedProps.valueType = props.valueType || null;
		if(typeof props.urlInputValue !== "undefined" || forceValue)
			formatedProps.urlInputValue = props.urlInputValue || "";

		// Return
		return formatedProps;

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = this.formateMutableProps(this.props, true);

		// Populate initial state with other
		initialState.isFocus = false;

		// Return state
		return initialState;

	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Datas
		var nextState = this.formateMutableProps(nextProps);

		// Set state
		this.setState(nextState);

	},

	// Render
	render: function() {

		// Datas
		var isFocus = this.state.isFocus;
		var action = this.props.action;
		var placeholder = this.props.placeholder;
		var value = this.state.value || "";
		var valueType = this.state.valueType;
		var fileName = this.props.fileName || "";
		var fileInputAction = this.props.fileInputAction;
		var fileInputDetails = this.props.fileInputDetails;
		var urlName = this.props.urlName || null;
		var urlInputAction = this.props.urlInputAction;
		var urlInputDetails = this.props.urlInputDetails;
		var urlInputValue = this.state.urlInputValue || "";
		var urlInputPlaceholder = this.props.urlInputPlaceholder;
		var classes = classNames("form-control", "form-file", this.props.className);
			
		// Return
		return(
			<div className={classes}>
				<div className="t-row">

					<div className="t-cell">

						<Button
							type="white"
							dropdown
							onClick={this._onButtonClick}
							ref="button">

							<span>{action}</span>

							<Dropdown helperPosition={'belowLeft'} position="belowLeft" className="form-file-dropdown" onShow={this._onFocus} onHide={this._onBlur} isShown={isFocus}>

								<ul className="v-list">
									<li className="file-item">
										<Button type="link" onClick={this._onFileButtonClick}>
											<span className="action">{fileInputAction}</span>
											<span className="details">{fileInputDetails}</span>
										</Button>
									</li>
									<li className="url-item">
										<span className="action">{urlInputAction}</span>
										<span className="details">{urlInputDetails}</span>
										<ControlText
											type="text"
											value={urlInputValue}
											placeholder={urlInputPlaceholder}
											onChange={this._onUrlInputChange}
											onEnterKeyPress={this._onUrlAdd} />
									</li>
								</ul>

								<input
									className="hidden-input"
									type="file"
									reRenderToken={valueType === "url" ? "url" : "file"}
									name={fileName}
									onChange={this._onFileInputChange}
									ref="fileInput" />

							</Dropdown>

						</Button>

					</div>
				
					<div className="t-cell max-width">
						<ControlText
							type="text"
							value={value}
							readOnly={true}
							placeholder={placeholder} />
					</div>
						
				</div>
			</div>
		);

	},

	// On file button click
	_onFileButtonClick: function(e) {

		// Open file input dialog
		ReactDOM.findDOMNode(this.refs.fileInput).click();

	},

	// On file input change
	_onFileInputChange: function(e) {

		// Datas
		var file = e.currentTarget.files[0];

		// If file is defined
		if(file) {

			// File add
			if(_.isFunction(this.props.onFileAdd)) {
				this.props.onFileAdd(file, e);
			}

			// Check if event default is prevented
			if(e.defaultPrevented) {
				return;
			}

			// Set state
			this.setState({
				urlInputValue: null,
				valueType: "file",
				value: file.name
			});

			// Blur
			this._onBlur(e);

		} else {

			// File add
			if(_.isFunction(this.props.onFileAdd)) {
				this.props.onFileAdd(null, e);
			}

			// Check if event default is prevented
			if(e.defaultPrevented) {
				return;
			}

			// Set state
			this.setState({
				urlInputValue: null,
				valueType: "file",
				value: null
			});

			// Blur
			this._onBlur(e);

		}

	},

	// On url input change
	_onUrlInputChange: function(value) {

		// Url change
		if(_.isFunction(this.props.onUrlChange)) {
			this.props.onUrlChange(value, e);
		}

		// Check if event default is prevented
		if(e.defaultPrevented) {
			return;
		}

		// Set state
		this.setState({
			urlInputValue: value
		});

	},

	// On url input change
	_onUrlAdd: function() {

		// Datas
		var urlInputValue = this.state.urlInputValue;

		// Url add
		if(_.isFunction(this.props.onUrlAdd)) {
			this.props.onUrlAdd(urlInputValue, e);
		}

		// Check if event default is prevented
		if(e.defaultPrevented) {
			return;
		}

		// Set state
		this.setState({
			urlInputValue: null,
			valueType: "url",
			value: urlInputValue
		});

		// Blur
		this._onBlur(e);

	},

	// On focus function
	_onFocus: function(e) {

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

		// Check if event default is prevented
		if(e.defaultPrevented) {
			return;
		}

		// Set state
		this.setState({
			isFocus: true
		});

	},

	// On blur function
	_onBlur: function(e) {

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

		// Check if event default is prevented
		if(e.defaultPrevented) {
			return;
		}

		// Set state
		this.setState({
			isFocus: false
		});

	}

});


// Exports component
module.exports = FormFileInput;