var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : FormRadio
var FormRadio = React.createClass({

	// Prop types
	propTypes: {

		// Important
		type: React.PropTypes.string.isRequired,
		name: React.PropTypes.string,
		label: React.PropTypes.string,
		isChecked: React.PropTypes.bool,
		isInitialyChecked: React.PropTypes.bool,

		// Plus
		onCheck: React.PropTypes.func

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.isChecked === "undefined" && typeof this.props.isInitialyChecked !== "undefined") {
			initialState.isChecked = !!this.props.isInitialyChecked;
		} else if(typeof this.props.isChecked === "undefined") {
			initialState.isChecked = false;
		}

		// Return state
		return initialState;

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, [ 'type', 'name', 'label', 'isChecked', 'isInitialyChecked', 'onCheck', 'className' ]);
	},

	// Render
	render: function() {

		// Get (un)controlled data
		if(typeof this.state.isChecked !== "undefined") {
			var isChecked = this.state.isChecked;
		} else {
			var isChecked = !!this.props.isChecked;
		}

		// Datas
		var name = this.props.name || null;
		var label = this.props.label || null;
		var classes = classNames("form-control", "form-radio", this.props.className, { checked: isChecked });
		var componentAttributes = this._getComponentAttributes(this.props);
			
		// Return
		return(
			<div className={classes} onClick={this._onCheck}>
				<span className="radio-icon"></span>
				{ label
					? <span className="radio-label">{label}</span>
					: false
				}
				<input type="hidden" name={name} value={isChecked} />
			</div>
		);

	},

	// On change
	_onCheck: function(e) {

		// If controlled data
		if(typeof this.props.isChecked !== "undefined") {

			// Check if already checked
			if(this.props.isChecked) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already checked
			if(this.state.isChecked) {
				return;
			}

			// Set state
			this.setState({
				isChecked: true
			});

		}

		// Check
		if(_.isFunction(this.props.onCheck)) {
			this.props.onCheck(e);
		}

	}

});


// Exports component
module.exports = FormRadio;