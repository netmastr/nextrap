var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : Static
var Static = React.createClass({

	// Render
	render: function() {

		// Data
		var children = this.props.children;
		var value = this.props.value;
		var classes = classNames("form-control", "form-static", this.props.className);
			
		// Return
		return(
			<span className={classes}>
				{typeof value !== "undefined" ? value : children}
			</span>
		);

	}

});


// Exports component
module.exports = Static;