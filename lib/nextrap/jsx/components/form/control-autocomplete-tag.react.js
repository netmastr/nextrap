var React = require('react');
var _ = require('underscore');
var classNames = require('classnames');


// Components
var ContextualLayer = require('../contextual/layer.react');
var ButtonList = require('../elements/button-list.react');
var ButtonListItem = require('../elements/button-list-item.react');
var Avatar = require('../elements/avatar.react');


// Set component : ControlAutocompleteTag
var ControlAutocompleteTag = React.createClass({

	// Instance data
	_displayedOptions: [],
	_inputRef: null,
	_lastInputValue: null,

	// Prop types
	propTypes: {

		// Details
		type: React.PropTypes.oneOf([ 'text', 'email', 'url' ]),
		name: React.PropTypes.string,
		placeholder: React.PropTypes.string,
		values: React.PropTypes.arrayOf(React.PropTypes.any),
		initialValues: React.PropTypes.arrayOf(React.PropTypes.any),
		inputValue: React.PropTypes.string,
		initialInputValue: React.PropTypes.string,
		showState: React.PropTypes.bool,
		transparent: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		isFocus: React.PropTypes.bool,
		initialIsFocus: React.PropTypes.bool,

		// Options
		options: React.PropTypes.arrayOf(React.PropTypes.any),
		additionalOptions: React.PropTypes.arrayOf(React.PropTypes.any),
		optionsMaxLength: React.PropTypes.number,
		matcher: React.PropTypes.oneOfType([ React.PropTypes.func, React.PropTypes.bool ]),
		matcherMinLength: React.PropTypes.number,
		highlighter: React.PropTypes.oneOfType([ React.PropTypes.func, React.PropTypes.bool ]),

		// Events
		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func,
		onInputChange: React.PropTypes.func,
		onInputFocus: React.PropTypes.func,
		onInputBlur: React.PropTypes.func,

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state values
		if(typeof this.props.values === 'undefined' && typeof this.props.initialValues !== 'undefined') initialState.values = this.props.initialValues || [];
		else if(typeof this.props.values === 'undefined') initialState.values = [];

		// Set state inputValue
		if(typeof this.props.inputValue === 'undefined' && typeof this.props.initialInputValue !== 'undefined') initialState.inputValue = this.props.initialInputValue || '';
		else if(typeof this.props.inputValue === 'undefined') initialState.inputValue = '';

		// Set state isFocus
		if(typeof this.props.isFocus === 'undefined' && typeof this.props.initialIsFocus !== 'undefined') initialState.isFocus = !!this.props.initialIsFocus;
		else if(typeof this.props.isFocus === 'undefined') initialState.isFocus = false;

		// Set other states
		initialState.hoverOptionIndex = 0;

		// Return state
		return initialState;

	},

	// Before component mount
	componentWillMount: function() {

		// Set displayed options
		this._displayedOptions = _getDisplayedOptions(this.props, this.state);

		// Return component
		if(typeof this.props.component === 'function') this.props.component(this);

	},

	// Before component update
	componentWillUpdate: function(nextProps, nextState) {
		this._displayedOptions = _getDisplayedOptions(nextProps, nextState);
	},

	// After component update
	componentDidUpdate: function() {
		if(typeof this.props.component === 'function') this.props.component(this);
	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Check if next props inputValue is defined and changed
		if(typeof nextProps.inputValue !== 'undefined' && nextProps.inputValue !== this.props.inputValue) {
			this.setState({ hoverOptionIndex: 0 });
		}

	},

	// Before component unmount
	componentWillUnmount: function() {
		if(typeof this.props.component === 'function') this.props.component(null);
	},

	// Render
	render: function() {

		// Data
		var self = this;
		var interactiveData = _getInteractiveData(this.props, this.state);
		var isFocus = interactiveData.isFocus;
		var isLayerHidden = !(isFocus && this._displayedOptions.length > 0);
		var type = this.props.type || 'text';
		var name = this.props.name || null;
		var placeholder = this.props.placeholder || null;
		var values = interactiveData.values;
		var inputValue = interactiveData.inputValue;
		var showState = !!this.props.showState;
		var readOnly = !!this.props.readOnly;
		var options = this.props.options instanceof Array ? this.props.options : [];
		var additionalOptions = this.props.additionalOptions instanceof Array ? this.props.additionalOptions : [];
		var optionsMaxLength = this.props.optionsMaxLength || 10;
		var matcher = this.props.matcher;
		var matcherMinLength = this.props.matcherMinLength || 0;
		var highlighter = this.props.highlighter;
		var classes = classNames('form-control', 'form-autocomplete-tag', this.props.className, {
			'read-only': readOnly,
			focus: isFocus,
			transparent: this.props.transparent
		});
		var layerClasses = classNames('form-autocomplete-tag-layer', {
			transparent: this.props.transparent,
		}, this.props.layerClassName);
		var hoverOptionIndex = this.state.hoverOptionIndex;
		var displayedOptions = this._displayedOptions;
		var optionSeparationAdded = null;

		// Get main displayedOptions
		var mainDisplayedOptions = _.filter(displayedOptions, function(obj) { return !obj.isAdditionalOption });
		var additionalDisplayedOptions = _.filter(displayedOptions, function(obj) { return obj.isAdditionalOption });

		// Get selected options
		var selectedOptions = {};

		// Options values
		_.each(options, function(obj) {
			if(typeof obj === 'string') {
				selectedOptions[obj] = obj;
			} else if(typeof obj === 'object' && obj !== null && typeof obj.value !== 'undefined') {
				selectedOptions[obj.value] = obj;
			} 
		});

		// Sort select options
		selectedOptions = _.map(values, function(val) { return selectedOptions[val]; });
			
		// Return
		return(
			<div className={classes} onClick={this._handleFrameClick}>

				<div className='input-frame'>
					<ul className='h-list'>
						
						{_.map(selectedOptions, function(optionItem, optionIndex) {
							return (
								<li key={optionIndex} className='tag-item' onClick={self._handlePullItem.bind(self, optionItem.value)}>

									{ typeof optionItem === 'object' && optionItem.avatar
										? [
											<Avatar key='0' className='contact-avatar' size='icn' href={null} hrefTitle={optionItem.label} imageSrc={optionItem.avatar} inline />,
											<span key='1'>&nbsp;</span>
										]
										: null
									}

									<span>{typeof optionItem === 'object' ? optionItem.label : optionItem}</span>

									{/* typeof optionItem === 'object' && optionItem.subLabel
										? [
											<span key='0'>&nbsp;</span>,
											<span key='1' className='light'>{optionItem.subLabel}</span>
										]
										: null
									*/}

								</li>
							);
						})}

						{/* values.length === 0
							? (
								<li className='placeholder-item'>
									<span>{placeholder}</span>
								</li>
							)
							: null
						*/}

						<li className='input-item'>
							<input
								style={values.length === 0 ? { width: 200 } : {}}
								type={type}
								name={name}
								placeholder={values.length > 0 ? '' : placeholder}
								value={inputValue || ''}
								readOnly={readOnly}
								onChange={this._handleInputChange}
								onFocus={this._handleInputFocus}
								onBlur={this._handleInputBlur}
								onKeyPress={this._handleKeyPress}
								onKeyDown={this._handleKeyDown}
								onKeyUp={this._handleKeyUp}
								ref={function(c) { self._inputRef = c; }} />
						</li>

					</ul>
				</div>

				<ContextualLayer
					position='belowLeft'
					isHidden={isLayerHidden}
					fitToHelper>

					{ displayedOptions.length > 0
						? (
							<div className={layerClasses} onMouseDown={this._handleLayerMouseDown}>
								<div className='hide-scroll-y'>
									<div className='scroll-y'>
										{ (function() {

											// Get button
											var getButton = function(isAdditionalOption, optionItem, optionIndex) {

												// Data
												var buttonBeforeCount = isAdditionalOption ? mainDisplayedOptions.length : 0;
												var isActive = hoverOptionIndex === (buttonBeforeCount + optionIndex);
												var key = buttonBeforeCount + optionIndex;

												// If option is a string
												if(typeof optionItem === 'string') {
													return (
														<ButtonListItem
															active={isActive}
															key={key}
															onClick={this._handlePushItem.bind(this, optionItem)}>

															<span>{optionItem}</span>
														
														</ButtonListItem>
													);
												}

												// If option is an object
												else {
													return (
														<ButtonListItem
															active={isActive}
															key={key}
															onClick={typeof optionItem.onClick === 'function' ? optionItem.onClick : this._handlePushItem.bind(this, optionItem.value)}>
																
															{ optionItem.avatar
																? [
																	<Avatar key='0' className='contact-avatar' size='icn' href={null} hrefTitle={optionItem.label} imageSrc={optionItem.avatar} inline />,
																	<span key='1'>&nbsp;</span>
																]
																: null
															}

															{ (function() {

																// If highlight is defined
																if(typeof highlighter === 'function' && optionItem.affectedByHighlighter !== false) {
																	
																	// Data
																	var splitedLabel = highlighter(inputValue, optionItem.label);

																	// Return
																	return (
																		<span className='main-text'>
																			<span className='nohighlight'>{splitedLabel[0]}</span>
																			<span className='highlight'>{splitedLabel[1]}</span>
																			<span className='nohighlight'>{splitedLabel[2]}</span>
																		</span>
																	);

																}

																// If default highlight is requested
																else if(highlighter === true && optionItem.affectedByHighlighter !== false) {
																	
																	// Data
																	var splitedLabel = _defaultHighlighter(inputValue, optionItem.label);

																	// Return
																	return (
																		<span className='main-text'>
																			<span className='nohighlight'>{splitedLabel[0]}</span>
																			<span className='highlight'>{splitedLabel[1]}</span>
																			<span className='nohighlight'>{splitedLabel[2]}</span>
																		</span>
																	);

																}

																// If no highlight
																else {

																	// Return
																	return (
																		<span className='main-text'>{optionItem.label}</span>
																	);

																}

															})() }

															{ optionItem.subLabel
																? [
																	<span key='0'>&nbsp;</span>,
																	<span key='1' className='sub-text light'>{optionItem.subLabel}</span>
																]
																: null
															}

														</ButtonListItem>
													);
												}
											};

											// Return
											return [
												mainDisplayedOptions.length > 0
													? (
														<ButtonList className='main-option-list' key={0}>
															{ mainDisplayedOptions.map(getButton.bind(this, false)) }
														</ButtonList>
													)
													: null
												,
												additionalDisplayedOptions.length > 0
													? (
														<ButtonList className='additional-option-list' key={1}>
															{ additionalDisplayedOptions.map(getButton.bind(this, true)) }
														</ButtonList>
													)
													: null
											];

										}).call(this) }
									</div>
								</div>
							</div>
						)
						: null
					}

				</ContextualLayer>

			</div>
		);

	},

	/* PRIVATE */

		// Handle frame click
		_handleFrameClick: function(e) {

			// Stop if not input zone
			if(this._inputRef === e.target) return;

			// Focus
			this._inputRef.focus();

		},

		// Handle layer mouse down
		_handleLayerMouseDown: function(e) {
			e.preventDefault();
		},

		// Handle push item
		_handlePushItem: function(value) {

			// Stop if read only
			if(this.props.readOnly) return;

			// Transform arguments
			value = value || '';

			// Set data
			var newState = {
				hoverOptionIndex: 0
			};

			// If uncontrolled data
			if(typeof this.props.values === 'undefined') {

				// Set data
				var newValues = _.clone(this.state.values);
					newValues.push(value);

				// Set new state
				newState.values = newValues;

			} else {

				// Set data
				var newValues = _.clone(this.props.values);
					newValues.push(value);

			}

			// If uncontrolled data
			if(typeof this.props.inputValue === 'undefined') {
				newState.inputValue = '';
			}

			// Set state
			this.setState(newState);

			// Input change
			// this._inputChange(label);

			// On change
			if(typeof this.props.onChange === 'function') this.props.onChange(newValues);

		},

		// Handle pull item
		_handlePullItem: function(value) {

			// Stop if read only
			if(this.props.readOnly) return;

			// Transform arguments
			value = value || '';

			// If uncontrolled data
			if(typeof this.props.values === 'undefined') {

				// Set data
				var newValues = _.clone(this.state.values);
					newValues = _.without(newValues, value);

				// Set state
				this.setState({
					values: newValues,
					inputValue: '',
					hoverOptionIndex: 0
				});

			}

			// If controlled data
			else {

				// Set data
				var newValues = _.clone(this.props.values);
					newValues = _.without(newValues, value);

			}

			// Input change
			// this._inputChange(label);

			// On change
			if(typeof this.props.onChange === 'function') this.props.onChange(newValues);

		},

		// Handle focus
		_handleFocus: function(e) {

			// Stop if read only
			if(this.props.readOnly) return;

			// If uncontrolled data
			if(typeof this.props.isFocus === 'undefined') {
				this.setState({ isFocus: true });
			}

			// On focus
			if(typeof this.props.onFocus === 'function') this.props.onFocus(e);

		},

		// Handle blur
		_handleBlur: function(e) {

			// Stop if read only
			if(this.props.readOnly) return;

			// If uncontrolled data
			if(typeof this.props.isFocus === 'undefined') {
				this.setState({ isFocus: false });
			}

			// On blur
			if(typeof this.props.onBlur === 'function') this.props.onBlur(e);

		},

		// Handle input change
		_handleInputChange: function(e) {

			// Set data
			var currentInputValue = _getInteractiveData(this.props, this.state).inputValue;
			var value = e.currentTarget.value || '';

			// Set last input value
			this._lastInputValue = currentInputValue;

			// Input change
			this._inputChange(value, e);

		},

		// Input change
		_inputChange: function(value, e) {

			// Stop if read only
			if(this.props.readOnly) return;

			// If uncontrolled data
			if(typeof this.props.inputValue === 'undefined') {
				this.setState({ inputValue: value, hoverOptionIndex: 0 });
			}

			// On input change
			if(typeof this.props.onInputChange === 'function') this.props.onInputChange(value, e);

		},

		// Handle input focus function
		_handleInputFocus: function(e) {

			// Stop if read only
			if(this.props.readOnly) return;

			// Set data
			var inputElement = this._inputRef;

			// Fix cursor position
			inputElement.selectionStart = inputElement.selectionEnd = inputElement.value.length;

			// On input focus
			if(typeof this.props.onInputFocus === 'function') this.props.onInputFocus(e);

			// Focus
			this._handleFocus(e);

		},

		// Handle blur function
		_handleInputBlur: function(e) {

			// Stop if read only
			if(this.props.readOnly) return;

			// On input blur
			if(typeof this.props.onInputBlur === 'function') this.props.onInputBlur(e);

			// Blur
			this._handleBlur(e);

		},

		// Handle key press
		_handleKeyPress: function(e) {

			// If pressed key is ENTER
			if(e.which === 13) {

				// Set data
				var values = _getInteractiveData(this.props, this.state).values;
				var selectedOption = this._displayedOptions[this.state.hoverOptionIndex];

				// If selected option is an object with onClick params
				if(typeof selectedOption === 'object' && selectedOption !== null && typeof selectedOption.onClick === 'function') {
					selectedOption.onClick(selectedOption.value, selectedOption.label);
				}

				// If selected option is an object
				else if(typeof selectedOption === 'object' && selectedOption !== null) {
					this._handlePushItem(selectedOption.value);
				}

				// If selected option is not an object
				else if(typeof selectedOption !== 'undefined' && selectedOption !== null) {
					this._handlePushItem(selectedOption);
				}

				// Prevent default
				return e.preventDefault();

			}

		},

		// Handle key down
		_handleKeyDown: function(e) {

			// If pressed key is UP
			if(e.which === 38) {

				// Check options length
				if(this.state.hoverOptionIndex === 0) {
					this.setState({ hoverOptionIndex: this._displayedOptions.length - 1 });
				} else {
					this.setState({ hoverOptionIndex: this.state.hoverOptionIndex - 1 });
				}

				// Prevent move of cursor
				return e.preventDefault();

			}

			// If pressed key is DOWN
			else if(e.which === 40) {

				// Check options length
				if(this.state.hoverOptionIndex === this._displayedOptions.length - 1) {
					this.setState({ hoverOptionIndex: 0 });
				} else {
					this.setState({ hoverOptionIndex: this.state.hoverOptionIndex + 1 });
				}

				// Prevent move of cursor
				return e.preventDefault();

			}

		},

		// Handle key up
		_handleKeyUp: function(e) {

			// If pressed key is DELETE
			if(e.which === 8) {

				// Set data
				var interactiveData = _getInteractiveData(this.props, this.state);

				// Get last input value
				var currentInputValue = interactiveData.inputValue;
				var lastInputValue = this._lastInputValue;

				// Set last input value
				this._lastInputValue = currentInputValue;

				// Stop if input value is not empty
				if(lastInputValue) return;

				// Set data
				var values = interactiveData.values;
				var selectedValue = _.last(values);

				// Check if an option is selected
				if(typeof selectedValue === 'undefined') return;

				// Change
				this._handlePullItem(selectedValue);
				this._inputChange('');

			}

		},

	/* /PRIVATE */

	/* PUBLIC */

		// Focus
		focus: function() {
			this._inputRef.focus();
		},

		// Blur
		blur: function() {
			this._inputRef.blur();
		}

	/* /PUBLIC */

});


// Default matcher function
var _defaultMatcher = function(value, item) {

	// Data
	value = value.toLowerCase();
	label = null;

	// Get label
	if(typeof item === 'string') {
		label = item.toLowerCase();
	} else if(typeof item === 'object') {
		label = item.label.toLowerCase();
	}
	
	// Return
	return label.indexOf(value) === 0;

};


// Default highlighter function
var _defaultHighlighter = function(value, label) {

	// Data
	var start = label.toLowerCase().indexOf(value.toLowerCase());
	var length = value.toLowerCase().length;
	var end = start + length;

	// If no highlight
	if(start !== 0) {
		return [ label, null, null ];
	}

	else {
		return [ null, label.substr(start, length), label.substr(end) ];
	}

};


// Get interactive data
var _getInteractiveData = function(props, state) {

	// Data
	var interactiveData = {};

	// Set value
	if(typeof props.values !== 'undefined') interactiveData.values = props.values || [];
	else interactiveData.values = state.values;

	// Set inputValue
	if(typeof props.inputValue !== 'undefined') interactiveData.inputValue = props.inputValue || '';
	else interactiveData.inputValue = state.inputValue;

	// Set isFocus
	if(typeof props.isFocus !== 'undefined') interactiveData.isFocus = !!props.isFocus;
	else interactiveData.isFocus = state.isFocus;

	// Return controlled props
	return interactiveData;

};


// Get displayed options function
var _getDisplayedOptions = function(props, state) {

	// Set data
	var interactiveData = _getInteractiveData(props, state);
	var values = interactiveData.values;
	var inputValue = interactiveData.inputValue;
	var matcher = props.matcher;
	var displayedOptions = props.options instanceof Array ? _.clone(props.options) : [];
	var additionalOptions = props.additionalOptions instanceof Array ? props.additionalOptions : [];
	var optionsMaxLength = props.optionsMaxLength || 10;

	// Filter already used options
	displayedOptions = _.filter(displayedOptions, function(obj) { 
		if(typeof obj === 'string') {
			return !_.contains(values, obj);
		} else if(typeof obj === 'object' && obj !== null && typeof obj.value !== 'undefined') {
			return !_.contains(values, obj.value);
		}
	});

	// Check matcher
	if(typeof matcher === 'function') {
		displayedOptions = _.filter(displayedOptions, matcher.bind(null, inputValue));
	} else if(matcher === true) {
		displayedOptions = _.filter(displayedOptions, _defaultMatcher.bind(null, inputValue));
	}

	// If options max length
	if(optionsMaxLength) {
		displayedOptions = displayedOptions.splice(0, optionsMaxLength);
	}

	// If additional options
	if(additionalOptions.length > 0) {

		// Add isAdditionalOption param
		additionalOptions = _.map(additionalOptions, function(obj) {
			obj.isAdditionalOption = true;
			return obj;
		});

		// Add to displayed options
		displayedOptions = displayedOptions.concat(additionalOptions);

	}

	// Return displayed options
	return displayedOptions;

};


// Exports component
module.exports = ControlAutocompleteTag;