var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ControlLabel
var ControlLabel = React.createClass({

	// Prop types
	propTypes: {},

	// Render
	render: function() {

		// Data
		var classes = classNames("form-control", "form-label", this.props.className);
			
		// Return
		return(
			<span className={classes}>
				{this.props.children}
			</span>
		);

	}

});


// Exports component
module.exports = ControlLabel;