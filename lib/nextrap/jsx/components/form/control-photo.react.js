var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Get components
var Button = require('../elements/button.react');
var ButtonList = require('../elements/button-list.react');
var ButtonListItem = require('../elements/button-list-item.react');
var Dropdown = require('../contextual/dropdown.react');
var ControlText = require('./control-text.react');


// Set component : FormPhotoInput
var FormPhotoInput = React.createClass({

	// Instance data
	_fileInput: null,

	// Prop types
	propTypes: {

		// Important
		type: React.PropTypes.string,
		value: React.PropTypes.string,
		initialValue: React.PropTypes.string,
		valueType: React.PropTypes.string,
		initialValueType: React.PropTypes.string,
		readOnly: React.PropTypes.bool,
		isLoading: React.PropTypes.bool,

		// File
		fileInputAction: React.PropTypes.string,
		fileInputDetails: React.PropTypes.string,

		// Url
		urlInputAction: React.PropTypes.string,
		urlInputDetails: React.PropTypes.string,
		urlInputPlaceholder: React.PropTypes.string,
		urlInputValue: React.PropTypes.string,
		initialUrlInputValue: React.PropTypes.string,

		// Loading
		loadingFileName: React.PropTypes.string,
		loadingFileSize: React.PropTypes.string,
		loadingProgress: React.PropTypes.number,

		// Plus
		className: React.PropTypes.string,
		onFileAdd: React.PropTypes.func,
		onUrlAdd: React.PropTypes.func,
		onUrlInputChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onBlur: React.PropTypes.func,
		isFocus: React.PropTypes.bool,
		isInitialyFocus: React.PropTypes.bool,
		blurOnChange: React.PropTypes.bool,
		dropdownProps: React.PropTypes.object,

	},

	// Default props
	getDefaultProps: function() {
		return {

			// Default
			type: 'all',

			// File
			fileInputAction: 'Upload a photo',
			fileInputDetails: 'Add an photo from your computer',

			// Url
			urlInputAction: 'Provide an url',
			urlInputDetails: 'Add a photo from the web',
			urlInputPlaceholder: 'http://mywebsite.com/my_photo.jpg',

		};
	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.value === 'undefined' && typeof this.props.initialValue !== 'undefined') {
			initialState.value = this.props.initialValue || '';
		} else if(typeof this.props.value === 'undefined') {
			initialState.value = '';
		}
		if(typeof this.props.valueType === 'undefined' && typeof this.props.initialValueType !== 'undefined') {
			initialState.valueType = this.props.initialValueType || null;
		} else if(typeof this.props.valueType === 'undefined') {
			initialState.valueType = null;
		}
		if(typeof this.props.urlInputValue === 'undefined' && typeof this.props.initialUrlInputValue !== 'undefined') {
			initialState.urlInputValue = this.props.initialUrlInputValue || '';
		} else if(typeof this.props.urlInputValue === 'undefined') {
			initialState.urlInputValue = '';
		}
		if(typeof this.props.isFocus === 'undefined' && typeof this.props.isInitialyFocus !== 'undefined') {
			initialState.isFocus = !!this.props.isInitialyFocus;
		} else if(typeof this.props.isFocus === 'undefined') {
			initialState.isFocus = false;
		}

		// Set component state
		initialState.urlInputIsDisplayed = false;

		// Return state
		return initialState;

	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Set state if focus is false
		if(nextProps.isFocus === false) {
			this.setState({
				urlInputIsDisplayed: false
			});
		}

	},

	// Render
	render: function() {

		// Get (un)controlled data
		if(typeof this.state.value !== 'undefined') {
			var value = this.state.value;
		} else {
			var value = this.props.value || '';
		}
		if(typeof this.state.valueType !== 'undefined') {
			var valueType = this.state.valueType;
		} else {
			var valueType = this.props.valueType || null;
		}
		if(typeof this.state.urlInputValue !== 'undefined') {
			var urlInputValue = this.state.urlInputValue;
		} else {
			var urlInputValue = this.props.urlInputValue || '';
		}
		if(typeof this.state.isFocus !== 'undefined') {
			var isFocus = this.state.isFocus;
		} else {
			var isFocus = !!this.props.isFocus;
		}

		// Datas
		var self = this;
		var type = this.props.type;
		var action = this.props.action;
		var details = this.props.details || '';
		var placeholderImage = this.props.placeholderImage || null;
		var fileInputAction = this.props.fileInputAction;
		var fileInputDetails = this.props.fileInputDetails;
		var urlInputAction = this.props.urlInputAction;
		var urlInputDetails = this.props.urlInputDetails;
		var urlInputPlaceholder = this.props.urlInputPlaceholder;
		var urlInputIsDisplayed = this.state.urlInputIsDisplayed;
		var isLoading = this.props.isLoading;
		var loadingProgress = Math.round(this.props.loadingProgress*100);
		var loadingFileName = this.props.loadingFileName || 'Unknown file';
		var loadingFileSize = this.props.loadingFileSize || '- bytes';
		var readOnly = this.props.readOnly;
		var dropdownProps = this.props.dropdownProps || {};
		var classes = classNames('form-control', 'form-photo', this.props.className);

		// If read only
		if(readOnly) {
			return <div />;
		}
			
		// Return
		return(
			<div className={classes}>
				<Button type='blank'>
					
					{this.props.children}

					<Dropdown
						{...dropdownProps}
						helperPosition={dropdownProps.helperPosition || 'belowLeft'}
						position={dropdownProps.position || 'belowLeft'}
						className={classNames('form-photo-dropdown', dropdownProps.className)}
						onShow={this._onFocus}
						onHide={this._onBlur}
						isShown={isFocus}
					>

						{(function() {

							if(urlInputIsDisplayed && !isLoading) {
								return (
									<div className='url-input-view'>
										<div className='t-group'>
											<div className='t-cell marged max-width align-middle'>
												<ControlText
													type='text'
													value={urlInputValue}
													placeholder={urlInputPlaceholder}
													onChange={self._onUrlInputChange}
													onEnterKeyPress={self._onUrlAdd} />
											</div>
											<div className='t-cell align-middle'>
												<Button type='blue' onClick={self._onUrlAdd}><span>OK</span></Button>
											</div>
										</div>
									</div>
								);
							}

							else if(!isLoading) {
								return (
									<ButtonList separatedByBorder>

										{ type === 'all' || type === 'file'
											? (
												<ButtonListItem className='file-item' onClick={self._onFileButtonClick}>
													<span className='action'>{fileInputAction}</span>
													<span className='action-details'>{fileInputDetails}</span>
												</ButtonListItem>
											)
											: null
										}

										{ type === 'all' || type === 'url'
											? (
												<ButtonListItem className='url-item' onClick={self._onUrlButtonClick}>
													<span className='action'>{urlInputAction}</span>
													<span className='action-details'>{urlInputDetails}</span>
												</ButtonListItem>
											)
											: null
										}

									</ButtonList>
								);
							}

							else if(isLoading) {
								return (
									<div className='loading-view'>

										<div className='t-group details'>
											<div className='t-cell marged max-width align-middle'>
												<span><div className='text-ellipsis'><span>{loadingFileName}</span></div></span>
											</div>
											<div className='t-cell align-middle'>
												<span className='subtle nowrap'>{loadingFileSize}</span>
											</div>
										</div>

										<div className='progress-bar'>
											<div className='progress-content' style={{ width: loadingProgress + '%' }} />
										</div>

									</div>
								);
							}

						})()}

						<input
							className='hidden-input'
							type='file'
							onChange={this._onFileInputChange}
							ref={function(c) { self._fileInput = c; }} />

					</Dropdown>

				</Button>
			</div>
		);

	},

	// On file button click
	_onFileButtonClick: function(e) {

		// Open file input dialog
		ReactDOM.findDOMNode(this._fileInput).click();

	},

	// On url button click
	_onUrlButtonClick: function(e) {

		// Open file input dialog
		this.setState({
			urlInputIsDisplayed: true
		});

	},

	// On file input change
	_onFileInputChange: function(e) {

		// Datas
		var file = e.currentTarget.files[0] || null;

		// If uncontrolled data
		if(typeof this.props.value === 'undefined') {

			// Set state
			this.setState({
				urlInputValue: null,
				valueType: 'file',
				value: null
			});

		}

		// File add
		if(_.isFunction(this.props.onFileAdd)) {
			this.props.onFileAdd(file, e);
		}

		// Blur
		if(this.props.blurOnChange) {
			this._onBlur(e);
		}

	},

	// On url input change
	_onUrlInputChange: function(value) {

		// If uncontrolled data
		if(typeof this.props.value === 'undefined') {

			// Set state
			this.setState({
				urlInputValue: value
			});

		}

		// Url input change
		if(_.isFunction(this.props.onUrlInputChange)) {
			this.props.onUrlInputChange(value, e);
		}

	},

	// On url input change
	_onUrlAdd: function(e) {

		// Datas
		var urlInputValue = this.state.urlInputValue;

		// If uncontrolled data
		if(typeof this.props.value === 'undefined') {

			// Set state
			this.setState({
				urlInputValue: null,
				valueType: 'url',
				value: null
			});

		}

		// Url add
		if(_.isFunction(this.props.onUrlAdd)) {
			this.props.onUrlAdd(urlInputValue, e);
		}

		// Blur
		if(this.props.blurOnChange) {
			this._onBlur(e);
		}

	},

	// On focus function
	_onFocus: function(e) {

		// If controlled data
		if(typeof this.props.isFocus !== 'undefined') {

			// Check if already focus
			if(this.props.isFocus || this.props.isLoading || this.props.readOnly) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already focus
			if(this.state.isFocus || this.props.isLoading || this.props.readOnly) {
				return;
			}

			// Set state
			this.setState({
				isFocus: true
			});

		}

		// Focus
		if(_.isFunction(this.props.onFocus)) {
			this.props.onFocus(e);
		}

	},

	// On blur function
	_onBlur: function(e) {

		// If controlled data
		if(typeof this.props.isFocus !== 'undefined') {

			// Check if already hide
			if(!this.props.isFocus) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already hide
			if(!this.state.isFocus) {
				return;
			}

			// Set state
			this.setState({
				isFocus: false,
				urlInputIsDisplayed: false
			});

		}

		// Blur
		if(_.isFunction(this.props.onBlur)) {
			this.props.onBlur(e);
		}

	}

});


// Exports component
module.exports = FormPhotoInput;