var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Get components
var Button = require('../elements/button.react');
var ButtonGroup = require('../elements/button-group.react');


// Set component : FormSelectButtons
var FormSelectButtons = React.createClass({

	// Prop types
	propTypes: {

		// Important
		name: React.PropTypes.string,
		value: React.PropTypes.string,
		initialValue: React.PropTypes.string,
		options: React.PropTypes.array,
		readOnly: React.PropTypes.bool,

		// Plus
		className: React.PropTypes.string,
		onChange: React.PropTypes.func

	},

	// Default props
	getDefaultProps: function() {
		return {

			// Important
			options : []

		};
	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.value === "undefined" && typeof this.props.initialValue !== "undefined") {
			initialState.value = this.props.initialValue || "";
		} else if(typeof this.props.value === "undefined") {
			initialState.value = "";
		}

		// Return state
		return initialState;

	},

	// Render
	render: function() {

		// Get (un)controlled data
		if(typeof this.state.value !== "undefined") {
			var value = this.state.value;
		} else {
			var value = this.props.value || "";
		}

		// Datas
		var name = this.props.name || null;
		var options = this.props.options;
		var readOnly = !!this.props.readOnly;
		var classes = classNames("form-control", "form-select-buttons", this.props.className);
			
		// Return
		return(
			<div className={classes}>

				<ButtonGroup>

					{ options.map(function(optionItem) {

						// Data
						var buttonType = value === optionItem.value ? "blue" : "white";

						// Return
						return (
  							<Button
  								type={buttonType}
  								disabled={readOnly}
  								onClick={this._onChange.bind(this, optionItem.value)}
  								key={optionItem.value || ""}>

  								<span>{optionItem.label}</span>
  								
  							</Button>
	  					);

					}.bind(this)) }

	  			</ButtonGroup>

	  			<input type="hidden" name={name} value={value} />
	  			
	  		</div>
		);

	},

	// On change
	_onChange: function(value) {

		// Check if read only
		if(this.props.readOnly) {
			return;
		}

		// Datas
		value = value || "";

		// If uncontrolled data
		if(typeof this.props.value === "undefined") {

			// Set state
			this.setState({
				value: value
			});

		}

		// Change
		if(_.isFunction(this.props.onChange)) {
			this.props.onChange(value, e);
		}

	}

});


// Exports component
module.exports = FormSelectButtons;