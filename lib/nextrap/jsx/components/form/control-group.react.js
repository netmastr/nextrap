var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ControlLabel
var ControlLabel = React.createClass({

	// Prop types
	propTypes: {},

	// Render
	render: function() {

		// Data
		var classes = classNames('form-control-group', this.props.className);
		var disposition = this.props.disposition ? this.props.disposition.split('-') : null;
			
		// Return
		return(
			<div className={classes}>
				<div className='t-group'>
					{React.Children.map(this.props.children, function(childItem, childIndex) {
						return (
							<div className='t-cell' style={(disposition && disposition[childIndex]) ? { width: (disposition[childIndex] + '%') } : null}>{childItem}</div>
						);
					})}
				</div>
			</div>
		);

	}

});


// Exports component
module.exports = ControlLabel;