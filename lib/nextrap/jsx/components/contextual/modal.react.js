var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Components
var ContextualWindow = require('./window.react');
var ContextualLayerGroup = require('./layer-group/component.react');
var Button = require('../elements/button.react');


// Set component : Modal
var Modal = React.createClass({

	// Instance data
	_modalRef: null,

	// Prop types
	propTypes: {
		light: React.PropTypes.bool,
		maxHeight: React.PropTypes.bool,
		children: React.PropTypes.element,
		parentSelector: React.PropTypes.func,
		onOpen: React.PropTypes.func,
		onClose: React.PropTypes.func,
		isOpen: React.PropTypes.bool,
		initialIsOpen: React.PropTypes.bool
	},

	// Prop types
	getDefaultProps: function() {
		return {
			light: true,
			maxHeight: false,
			parentSelector: function(helper) {
				return window.document.getElementById('mainContextualWindowGroup');
			}
		};
	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set state
		if(typeof this.props.isOpen === 'undefined' && typeof this.props.initialIsOpen !== 'undefined') initialState.isOpen = !!this.props.initialIsOpen;
		else if(typeof this.props.isOpen === 'undefined') initialState.isOpen = false;

		// Return state
		return initialState;

	},

	// Get component attributes
	_getComponentProps: function(props) {
		return _.omit(this.props, _.keys(Modal.propTypes), 'className');
	},

	// After component mount
	componentDidMount: function () {

		// Add body click listener
		document.body.addEventListener('click', this.handleBodyClick);
	      
	},

	// Before component unmount
	componentWillUnmount: function () {

		// Remove body click listener
		document.body.removeEventListener('click', this.handleBodyClick);
	      
	},

	// Render
	render: function() {

		// Set (un)controlled data
		var isOpen = typeof this.state.isOpen !== 'undefined' ? this.state.isOpen : !!this.props.isOpen;

		// Set data
		var self = this;
		var light = this.props.light;
		var maxHeight = this.props.maxHeight;
		var parentSelector = this.props.parentSelector;
		var children = this.props.children;
		var componentProps = this._getComponentProps(this.props);
		var classes = classNames('modal', this.props.className);

		// Render
		return(
			<ContextualWindow
				isHidden={!isOpen}
				light={light}
				maxHeight={maxHeight}
				parentSelector={parentSelector}
				onHelperClick={this.handleHelperClick}>

				<div {...componentProps} className={classes} ref={function(c) { self._modalRef = c; }}>

					<div className='modal-close'>
						<Button type='icon' className='btn-close' onClick={this.handleClose}>
							<span className='icn icn-close'></span>
						</Button>
					</div>

					<div className='modal-content'>
						{children}
					</div>

					<ContextualLayerGroup />

				</div>

			</ContextualWindow>
		);

	},

	// Handle close
	handleClose: function(e) {

		// If controlled data
		if(typeof this.props.isOpen !== 'undefined') {

			// Check if already close
			if(!this.props.isOpen) {
				return;
			}

		}

		// If uncontrolled data
		else {

			// Check if already close
			if(!this.state.isOpen) {
				return;
			}

			// Set state
			this.setState({
				isOpen: false
			});

		}

		// Close
		if(_.isFunction(this.props.onClose)) {
			this.props.onClose(e);
		}
		
	},

	// Handle body click
	handleBodyClick: function(e) {

		// Return if modal is not defined
		if(!this._modalRef) {
			return;
		}

		// Set data
		var modalWindow = this._modalRef.parentNode;
		var modalWindowEnvironment = modalWindow.parentNode;

		// Stop if not close zone
		if(!(modalWindowEnvironment.contains(e.target) && modalWindow !== e.target && !modalWindow.contains(e.target))) return;

		// Close
		this.handleClose(e);
		
	},

	// Handle helper click
	handleHelperClick: function(e) {

		// If uncontrolled data
		if(typeof this.props.isOpen === 'undefined') {

			// Set state
			this.setState({
				isOpen: true
			});

		}

		// Open
		if(_.isFunction(this.props.onOpen)) {
			this.props.onOpen(e);
		}

	}

});


// Exports component
module.exports = Modal;