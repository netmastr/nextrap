var React = require('react');
var ReactDOM = require('react-dom');
var hat = require('hat');
var classNames = require('classnames');
var _ = require('underscore');


// Set component : ContextualWindow
var ContextualWindow = React.createClass({

	// Instance data data
	_helperRef: null,
	_parentRef: null,
	_windowRef: null,

	// Prop types
	propTypes: {
		isHidden: React.PropTypes.bool,
		light: React.PropTypes.bool,
		maxHeight: React.PropTypes.bool,
		children: React.PropTypes.element,
		parentSelector: React.PropTypes.func,
		onHelperClick: React.PropTypes.func
	},

	// Prop types
	getDefaultProps: function() {
		return {
			isHidden: false,
			light: false,
			maxHeight: false,
			parentSelector: function(helper) {
				return window.document.getElementById('reactApp');
			}
		};
	},

	// Get component attributes
	_getComponentProps: function(props) {
		return _.omit(this.props, _.keys(ContextualWindow.propTypes), 'className');
	},

	// After component mount
	componentDidMount: function() {

		// Set data
		var isHidden = this.props.isHidden;
		var light = this.props.light;
		var maxHeight = this.props.maxHeight;
		var children = this.props.children;
		var windowClasses = classNames('contextual-window-environment', { 'hidden-element': isHidden, light: light, 'max-height': maxHeight });

		// Set parent
		this._parentRef = this.props.parentSelector(this._helperRef);

		// Set window
		this._windowRef = window.document.createElement('div');
		this._windowRef.className = windowClasses;

		// Append window to parent
		this._parentRef.appendChild(this._windowRef);

		// Set window component
		var windowContent = (
			<div className='contextual-window'>
				{children}
			</div>
		);

		// Render window content to window
		ReactDOM.render(windowContent, this._windowRef);

	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Set data
		var isHidden = nextProps.isHidden;
		var light = nextProps.light;
		var maxHeight = nextProps.maxHeight;
		var children = nextProps.children;
		var windowClasses = classNames('contextual-window-environment', { 'hidden-element': isHidden, light: light, 'max-height': maxHeight });

		// Check if parent will change
		if(this.props.parentSelector(this._helperRef) !== nextProps.parentSelector(this._helperRef)) {
			this._parentRef = nextProps.parentSelector(this._helperRef);
			this._parentRef.appendChild(this._windowRef);
		}

		// Update window
		this._windowRef.className = windowClasses;

		// Set window component
		var windowComponent = (
			<div className='contextual-window'>
				{children}
			</div>
		);

		// Render window
		ReactDOM.render(windowComponent, this._windowRef);

	},

	// Before component unmount
	componentWillUnmount: function() {

		// Unmount window content
		var isUnmount = ReactDOM.unmountComponentAtNode(this._windowRef);

		// Remove window from parent
		if(isUnmount) {
			this._parentRef.removeChild(this._windowRef);
		}

	},

	// Render
	render: function() {

		// Datas
		var self = this;
		var componentProps = this._getComponentProps(this.props);
		var classes = classNames('contextual-window-helper', this.props.className);

		// Return
		return <div {...componentProps} className={classes} onClick={this.handleHelperClick} ref={function(c) { self._helperRef = c; }} />;

	},

	// handle helper click
	handleHelperClick: function(e) {
		if(_.isFunction(this.props.onHelperClick)) {
			this.props.onHelperClick(e);
		}
	}
	
});


// Exports component
module.exports = ContextualWindow;