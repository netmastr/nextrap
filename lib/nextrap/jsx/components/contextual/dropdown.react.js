var React = require('react');
var classNames = require('classnames');
var _ = require('underscore');


// Get components
var ContextualLayer = require('./layer.react');


// Set component
var Dropdown = React.createClass({

	// Instance data
	_helperRef: null,
	_dropdownRef: null,

	// Prop types
	propTypes: {

		// Important
		children: React.PropTypes.any,
		position: React.PropTypes.string,
		reference: React.PropTypes.string,
		helperPosition: React.PropTypes.string,
		isRelativeToArrow: React.PropTypes.bool,
		initialIsShown: React.PropTypes.bool,
		isShown: React.PropTypes.bool,

		// Events
		component: React.PropTypes.func,
		onShow: React.PropTypes.func,
		onHide: React.PropTypes.func

	},

	// Get initial state
	getInitialState: function() {

		// Datas
		var initialState = {};

		// Set layer shown status
		if(typeof this.props.isShown !== 'boolean' && typeof this.props.initialIsShown === 'boolean') initialState.isShown = this.props.initialIsShown;
		else if(typeof this.props.isShown !== 'boolean') initialState.isShown = false;

		// Return state
		return initialState;

	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, _.keys(Dropdown.propTypes), 'className');
	},

	// Before component mount
	componentWillMount: function() {
		if(typeof this.props.component === 'function') this.props.component(this);
	},

	// After component mount
	componentDidMount: function () {
		document.body.addEventListener('click', this._handleBodyClick);
	},

	// After component mount
	componentDidUpdate: function () {
		if(typeof this.props.component === 'function') this.props.component(this);
	},

	// Before component unmount
	componentWillUnmount: function () {

		// Return component
		if(typeof this.props.component === 'function') this.props.component(null);

		// Remove body click listener
		document.body.removeEventListener('click', this._handleBodyClick);
	      
	},

	// Render
	render: function() {

		// Set data
		var self = this;
		var interactiveData = _getInteractiveData(this.props, this.state);
		var isHidden = !interactiveData.isShown;
		var position = this.props.position || null;
		var reference = this.props.reference || null;
		var helperPosition = this.props.helperPosition || null;
		var isRelativeToArrow = !!this.props.isRelativeToArrow;
		var children = this.props.children;
		var componentAttributes = this._getComponentAttributes(this.props);
		var classes = classNames('dropdown', {
			belowleft: position === 'belowLeft' || position === null,
			belowright: position === 'belowRight',
			belowcenter: position === 'belowCenter',
			aboveleft: position === 'aboveLeft',
			aboveright: position === 'aboveRight',
			abovecenter: position === 'aboveCenter',
			beforetop: position === 'beforeTop',
			beforebottom: position === 'beforeBottom',
			beforemiddle: position === 'beforeMiddle',
			aftertop: position === 'afterTop',
			afterbottom: position === 'afterBottom',
			aftermiddle: position === 'afterMiddle'
		}, {
			'relative-to-arrow': isRelativeToArrow
		}, this.props.className);

		// Render
		return(
			<ContextualLayer
				position={helperPosition}
				reference={reference}
				isHidden={isHidden}
				onHelperClick={this._handleHelperClick}
				helperRef={function(c) { self._helperRef = c; }}
				component={function(c) { self._layer = c; }}>

				<div {...componentAttributes} className={classes} ref={function(c) { self._dropdownRef = c; }}>
					{children}
				</div>

			</ContextualLayer>
		);

	},

	/* PRIVATE */

		// Handle body click
		_handleBodyClick: function(e) {

			// Stop if not in hide zone
			if(!this._dropdownRef || this._dropdownRef === e.target || this._dropdownRef.contains(e.target) || this._helperRef === e.target) return;

			// Handle hide
			this._handleHide(e, true);
			
		},

		// Handle helper click
		_handleHelperClick: function(e) {

			// Handle show/hide
			var isShown = _getInteractiveData(this.props, this.state).isShown;
			if(isShown) this._handleHide(e, true);
			else this._handleShow(e, true);

		},

		// Handle show
		_handleShow: function(e, willPushEvent) {

			// Stop if already shown
			var isShown = _getInteractiveData(this.props, this.state).isShown;
			if(isShown) return;

			// If uncontrolled data
			if(typeof this.props.isShown !== 'boolean') {
				this.setState({ isShown: true });
			}

			// On show
			if(typeof this.props.onShow === 'function' && willPushEvent) this.props.onShow(e);

		},

		// Handle hide
		_handleHide: function(e, willPushEvent) {

			// Stop if already hide
			var isShown = _getInteractiveData(this.props, this.state).isShown;
			if(!isShown) return;

			// If uncontrolled data
			if(typeof this.props.isShown !== 'boolean') {
				this.setState({ isShown: false });
			}

			// On hide
			if(typeof this.props.onHide === 'function' && willPushEvent) this.props.onHide(e);

		},

	/* /PRIVATE */

	/* PUBLIC */

		// Show
		show: function(preventEventHandle) {
			this._handleShow(null, !preventEventHandle);
		},

		// Hide
		hide: function(preventEventHandle) {
			this._handleHide(null, !preventEventHandle);
		}

	/* /PUBLIC */

});


// Get interactive data
var _getInteractiveData = function(props, state) {

	// Data
	var interactiveData = {};

	// Set isShown
	if(typeof props.isShown === 'boolean') interactiveData.isShown = props.isShown;
	else interactiveData.isShown = state.isShown;

	// Return controlled props
	return interactiveData;

};


// Exports component
module.exports = Dropdown;