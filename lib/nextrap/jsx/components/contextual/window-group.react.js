var React = require('react');
var _ = require('underscore');
var classNames = require('classnames');


// Set component
var ContextualWindowGroup = React.createClass({

	// Render
	render: function() {

		// Set data
		var componentProps = _.omit(this.props, [ 'className']);
		var componentClasses = classNames('contextual-window-group', this.props.className);

		// Return
		return (
			<div className={componentClasses} {...componentProps} />
		);

	}

});


// Exports component
module.exports = ContextualWindowGroup;