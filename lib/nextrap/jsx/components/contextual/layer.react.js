var React = require('react');
var hat = require('hat');
var classNames = require('classnames');
var _ = require('underscore');


// Get actions
var ContextualLayerGroupActions = require('./layer-group/actions');


// Set component : ContextualLayer
var ContextualLayer = React.createClass({

	// Instance data
	_id: null,
	_helperRef: null,

	// Prop types
	propTypes: {

		// Important
		children: React.PropTypes.any,
		position: React.PropTypes.string,
		reference: React.PropTypes.string,
		fitToHelper: React.PropTypes.bool,
		helperRef: React.PropTypes.func,
		component: React.PropTypes.func,
		initialIsHidden: React.PropTypes.bool,
		isHidden: React.PropTypes.bool,

		// Events
		onHelperClick: React.PropTypes.func,
		onHelperMouseEnter: React.PropTypes.func,
		onHelperMouseLeave: React.PropTypes.func
		
	},

	// Get component attributes
	_getComponentAttributes: function(props) {
		return _.omit(this.props, _.keys(ContextualLayer.propTypes), 'className');
	},

	// Before component mount
	componentWillMount: function() {
		if(typeof this.props.component === 'function') this.props.component(this);
	},

	// After component mount
	componentDidMount: function() {

		// Set isHidden
		if(typeof this.props.isHidden === 'boolean') var isHidden = !!this.props.isHidden;
		else if(typeof this.props.initialIsHidden === 'boolean') var isHidden = !!this.props.initialIsHidden;
		else var isHidden = false;

		// Set instance properties
		this._id = hat();

		// Datas
		var id = this._id;
		var helper = this._helperRef;
		var children = this.props.children;
		var params = {
			position: this.props.position || null,
			reference: this.props.reference || 'auto',
			isHidden: isHidden,
			fitToHelper: !!this.props.fitToHelper
		};

		// Mount layer
		setTimeout(ContextualLayerGroupActions.mountLayer.bind(null, id, helper, children, params), 1);

	},

	// After component mount
	componentDidUpdate: function() {
		if(typeof this.props.component === 'function') this.props.component(this);
	},

	// Before component receive props
	componentWillReceiveProps: function(nextProps) {

		// Set data
		var id = this._id;
		var helper = this._helperRef;
		var children = nextProps.children;
		var params = {
			position: nextProps.position || null,
			reference: nextProps.reference || 'auto',
			fitToHelper: !!nextProps.fitToHelper
		};

		// Add isHidden to params if controlled data
		if(typeof nextProps.isHidden === 'boolean') {
			params.isHidden = !!nextProps.isHidden;
		}

		// Update layer
		setTimeout(ContextualLayerGroupActions.updateLayer.bind(null, id, helper, children, params), 1);

	},

	// Before component unmount
	componentWillUnmount: function() {

		// Unmount layer
		setTimeout(ContextualLayerGroupActions.unmountLayer.bind(null, this._id), 1);

		// Return component
		if(typeof this.props.component === 'function') this.props.component(null);

	},

	// Render
	render: function() {

		// Datas
		var componentAttributes = this._getComponentAttributes(this.props);
		var classes = classNames('contextual-layer-helper', this.props.className);

		// Return
		return (
			<div
				{...componentAttributes}
				className={classes}
				onClick={this._handleHelperClick}
				onMouseEnter={this._handleHelperMouseEnter}
				onMouseLeave={this._handleHelperMouseLeave}
				ref={this._handleHelperRef} />
		);

	},

	/* PRIVATE */

		// Handle helper ref
		_handleHelperRef: function(element) {

			// Set helper
			this._helperRef = element;

			// Helper click
			if(typeof this.props.helperRef === 'function') this.props.helperRef(element);

		},

		// Handle helper click
		_handleHelperClick: function(e) {
			if(typeof this.props.onHelperClick === 'function') this.props.onHelperClick(e);
		},

		// Handle helper mouse enter
		_handleHelperMouseEnter: function(e) {
			if(typeof this.props.onHelperMouseEnter === 'function') this.props.onHelperMouseEnter(e);
		},

		// Handle helper mouse leave
		_handleHelperMouseLeave: function(e) {
			if(typeof this.props.onHelperMouseLeave === 'function') this.props.onHelperMouseLeave(e);
		},

	/* /PRIVATE */

	/* PUBLIC */

		// Show
		show: function() {

			// Stop if controlled data
			if(typeof this.props.isHidden === 'boolean') return;

			// Toggle layer
			setTimeout(ContextualLayerGroupActions.toggleLayer.bind(null, this._id, true), 1);

		},

		// Hide
		hide: function() {

			// Stop if controlled data
			if(typeof this.props.isHidden === 'boolean') return;

			// Toggle layer
			setTimeout(ContextualLayerGroupActions.toggleLayer.bind(null, this._id, false), 1);

		}
	
	/* /PUBLIC */
	
});


// Exports component
module.exports = ContextualLayer;