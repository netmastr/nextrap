var Dispatcher = require('../../../../dispatcher');
var ContextualLayerGroupConstants = require('./constants');


// Set ContextualLayerGroupActions
var ContextualLayerGroupActions = {

	/**
	 * @param {string} id
	 * @param {component} helper
	 * @param {component} children
	 * @param {object} [params]
	 */
	mountLayer: function(id, helper, children, params) {
		Dispatcher.dispatch({
			actionType: ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_MOUNT_LAYER,
			id: id,
			helper: helper,
			children: children,
			params: params
		});
	},

	/**
	 * @param {string} id
	 * @param {component} [helper]
	 * @param {component} [children]
	 * @param {object} [params]
	 */
	updateLayer: function(id, helper, children, params) {
		Dispatcher.dispatch({
			actionType: ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UPDATE_LAYER,
			id: id,
			helper: helper,
			children: children,
			params: params
		});
	},

	/**
	 * @param null
	 */
	updateLayers: function() {
		Dispatcher.dispatch({
			actionType: ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UPDATE_LAYERS
		});
	},

	/**
	 * @param {string} id
	 */
	unmountLayer: function(id) {
		Dispatcher.dispatch({
			actionType: ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UNMOUNT_LAYER,
			id: id
		});
	},

	/**
	 * @param {string} id
	 * @param {boolean} willShow
	 */
	toggleLayer: function(id, willShow) {
		Dispatcher.dispatch({
			actionType: ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_TOGGLE_LAYER,
			id: id,
			willShow: willShow
		});
	}

};


// Module exports
module.exports = ContextualLayerGroupActions;