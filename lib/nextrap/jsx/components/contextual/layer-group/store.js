var EventEmitter = require('events').EventEmitter;
var Immutable = require('immutable');
var _ = require('underscore');
var assign = require('object-assign');
var objectPath = require('object-path');


// Get dispatcher and constants
var Dispatcher = require('../../../../dispatcher');
var ContextualLayerGroupConstants = require('./constants');


// Constants
var CHANGE_EVENT = 'change';

// Store
var _store = Immutable.fromJS({
	layers: {
		ids: [],
		hash: {}
	}
});


/* CONTEXTUAL-LAYER-GROUP */

	// Mount layer
	var mountLayer = function(id, helper, children, params) {

		// Stop if layer is already defined
		if(_store.getIn([ 'layers', 'hash', id ])) throw new Error('Layer already exists.');

		// Set data
		var position = objectPath.get(params, 'position') || null;
		var reference = objectPath.get(params, 'reference') || 'auto';
		var referenceElement = _getReferenceElement(reference, helper);
		var isHidden = !!objectPath.get(params, 'isHidden');
		var fitToHelper = !!objectPath.get(params, 'fitToHelper');

		// Update store
		_store = _store
			.updateIn([ 'layers', 'ids' ], function(obj) {
				return obj.push(id);
			})
			.setIn([ 'layers', 'hash', id ], Immutable.Map({
				id: id,
				helper: helper,
				position: position,
				children: children,
				reference: reference,
				referenceElement: referenceElement,
				isHidden: isHidden,
				fitToHelper: fitToHelper
			}));

		// Emit change
		ContextualLayerGroupStore.emitChange();

	};

	// Update layer
	var updateLayer = function(id, helper, children, params) {

		// Set data
		var _storeLayerItem = _store.getIn([ 'layers', 'hash', id ]);

		// Stop if layer is not defined
		if(!_storeLayerItem) return;

		// Set data
		var newLayerValues = {};

		// Set new values
		if(typeof helper !== 'undefined') newLayerValues.helper = helper;
		if(typeof children !== 'undefined') newLayerValues.children = children;
		if(typeof objectPath.get(params, 'position') !== 'undefined') newLayerValues.position = params.position || null;
		if(typeof objectPath.get(params, 'reference') !== 'undefined') newLayerValues.reference = params.reference || 'auto';
		if(typeof objectPath.get(params, 'isHidden') !== 'undefined') newLayerValues.isHidden = !!params.isHidden;
		if(typeof objectPath.get(params, 'fitToHelper') !== 'undefined') newLayerValues.fitToHelper = !!params.fitToHelper;
		if(typeof newLayerValues.reference !== 'undefined' && newLayerValues.reference !== _storeLayerItem.get('reference')) {
			newLayerValues.reference = _getReferenceElement(newLayerValues.reference, newLayerValues.helper || _storeLayerItem.get('helper'));
		}

		// Update store
		_store = _store
			.mergeIn([ 'layers', 'hash', id ], Immutable.Map(newLayerValues))
			.updateIn([ 'layers', 'ids' ], function(obj) {
				return obj
					.delete(obj.indexOf(id))
					.push(id);
			});

		// Emit change
		ContextualLayerGroupStore.emitChange();

	};

	// Update layers
	var updateLayers = function() {

		// Emit change
		ContextualLayerGroupStore.emitChange();

	};

	// Unmount layer
	var unmountLayer = function(id) {

		// Set data
		var _storeLayerItem = _store.getIn([ 'layers', 'hash', id ]);

		// Stop if layer is not defined
		if(!_storeLayerItem) return;

		// Update store
		_store = _store
			.deleteIn([ 'layers', 'hash', id ])
			.updateIn([ 'layers', 'ids' ], function(obj) {
				return obj.delete(obj.indexOf(id));
			});

		// Emit change
		ContextualLayerGroupStore.emitChange();

	};

	// Toggle layer
	var toggleLayer = function(id, willShow) {

		// Set data
		var _storeLayerItem = _store.getIn([ 'layers', 'hash', id ]);

		// Stop if layer is not defined
		if(!_storeLayerItem) return;

		// Update store
		_store = _store
			.setIn([ 'layers', 'hash', id, 'isHidden' ], !willShow)
			.updateIn([ 'layers', 'ids' ], function(obj) {
				return obj
					.delete(obj.indexOf(id))
					.push(id);
			});

		// Emit change
		ContextualLayerGroupStore.emitChange();

	};

/* /CONTEXTUAL-LAYER-GROUP */


/* PRIVATE */

	// Get reference element
	var _getReferenceElement = function(reference, helper) {

	 	// Data
	 	var i;
	 	var parentElement;
	 	var parentElementClassNames;
	 	var helperParents = $(helper).parents();
	 	var referenceElement = null;

	 	// If reference is auto
	 	if(reference === 'auto') {

	 		// Loop over helper parents
	 		for(i = 0; i < helperParents.length; i++) {

	 			// Data
	 			parentElement = helperParents[i];
	 			parentElementClassNames = parentElement.className.split(' ');

	 			// If scroll
	 			if(_.contains(parentElementClassNames, 'scroll-y') || _.contains(parentElementClassNames, 'scroll-x')) {
	 				referenceElement = parentElement.children[0];
	 				break;
	 			}

	 			// If window
	 			else if(_.contains(parentElementClassNames, 'contextual-window')) {
	 				referenceElement = parentElement.children[0];
	 				break;
	 			}

	 			// If content
	 			else if(parentElement.id === 'content') {
	 				referenceElement = parentElement.children[0];
	 				break;
	 			}

	 			// If body
	 			else if(parentElement.tagName.toLowerCase() === 'body') {
	 				referenceElement = parentElement.children[0].children[0];
	 				break;
	 			}

	 		}

	 	}

	 	// If reference is window
	 	else if(reference === 'scroll') {

	 		// Loop over helper parents
	 		for(i = 0; i < helperParents.length; i++) {

	 			// Data
	 			parentElement = helperParents[i];
	 			parentElementClassNames = parentElement.className.split(' ');

	 			// If scroll
	 			if(_.contains(parentElementClassNames, 'scroll-y') || _.contains(parentElementClassNames, 'scroll-x')) {
	 				referenceElement = parentElement.children[0];
	 				break;
	 			}

	 		}

			// Check if reference element was found
			if(referenceElement === null) {
				throw new Error('You\'re layer helper is not in a .scroll-y or .scroll-x. You can\'t use scroll as reference.')
			}

		}

	 	// If reference is window
	 	else if(reference === 'window') {

	 		// Loop over helper parents
	 		for(i = 0; i < helperParents.length; i++) {

	 			// Data
	 			parentElement = helperParents[i];
	 			parentElementClassNames = parentElement.className.split(' ');

	 			// If window
	 			if(_.contains(parentElementClassNames, 'contextual-window')) {
	 				referenceElement = parentElement.children[0];
	 				break;
	 			}

	 		}

			// Check if reference element was found
			if(referenceElement === null) {
				throw new Error('You\'re layer helper is not in a .contextual-window. You can\'t use window as reference.')
			}

		}

	 	// If reference is content
	 	else if(reference === 'content') {

	 		// Set reference element
	 		referenceElement = $('#content')[0].children[0];

		}

	 	// If reference is body
	 	else if(reference === 'body') {

	 		// Set reference element
	 		referenceElement = $('body')[0].children[0].children[0];

		}

		// Check if reference element was found
		if(referenceElement === null) {
			throw new Error('You\'re specify a wrong reference for you\'re layer.')
		}

		// Return reference element
		return referenceElement;

	};

/* /PRIVATE */


// Get ContextualLayerGroupStore
var ContextualLayerGroupStore = assign({}, EventEmitter.prototype, {

	// Get store
	getStore: function() {
		return _store;
	},

	// Change events
	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},
	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},
	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	// Dispatcher
	dispatcherIndex: Dispatcher.register(function(payload) {

		// Switch action
		switch(payload.actionType) {

			/* CONTEXTUAL-LAYER-GROUP */

				// Mount layer
				case ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_MOUNT_LAYER:
					mountLayer(payload.id, payload.helper, payload.children, payload.params);
					break;

				// Update layer
				case ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UPDATE_LAYER:
					updateLayer(payload.id, payload.helper, payload.children, payload.params);
					break;

				// Update layers
				case ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UPDATE_LAYERS:
					updateLayers();
					break;

				// Unmount layer
				case ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_UNMOUNT_LAYER:
					unmountLayer(payload.id);
					break;

				// Toggle layer
				case ContextualLayerGroupConstants.CONTEXTUAL_LAYER_GROUP_TOGGLE_LAYER:
					toggleLayer(payload.id, payload.willShow);
					break;

			/* /CONTEXTUAL-LAYER-GROUP */

		}

		// Return true if no error
		return true;

	})

});

module.exports = ContextualLayerGroupStore;