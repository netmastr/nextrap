var React = require('react');
var _ = require('underscore');
var assign = require('object-assign');
var classNames = require('classnames');
var hat = require('hat');


// Actions and store
var ContextualLayerGroupActions = require('./actions');
var ContextualLayerGroupStore = require('./store');


// Get store function
var getState = function() {
	return ContextualLayerGroupStore.getStore();
};


// Set component
var ContextualLayerGroup = React.createClass({

	// Instance data
	_id: null,
	_isMounted: null,
	_parentResizeInterval: null,
	_layerGroupRef: null,

	// Get initial state
	getInitialState: function() {
		return {
			data: getState(),
			referenceElement: null
		};
	},

	// Before component mount
	componentWillMount: function() {
		this._id = hat();
	},

	// After component mount
	componentDidMount: function() {

		// Set instance data
		this._isMounted = true;

		// Add event listeners
		ContextualLayerGroupStore.addChangeListener(this._onChange);
		window.addEventListener('resize', this._onWindowResize);

		// Set reference element
		this.setState({
			referenceElement: _getReferenceElement(this._layerGroupRef)
		});

		// Add parent resize listener
		this._parentResizeInterval = setInterval(this._onParentResize, 100);

	},

	// After component update
	componentDidUpdate: function() {

		// Set data
		var referenceElement = _getReferenceElement(this._layerGroupRef);

		// Set reference element
		if(referenceElement !== this.state.referenceElement) {
			this.setState({ referenceElement: referenceElement });
		}

	},

	// Before component unmount
	componentWillUnmount: function() {

		// Set instance data
		this._isMounted = false;

		// Remove event listeners
		ContextualLayerGroupStore.removeChangeListener(this._onChange);
		window.removeEventListener('resize', this._onWindowResize);

		// Remove parent resize listener
		clearInterval(this._parentResizeInterval);

	},

	// Render
	render: function() {

		// Set data
		var self = this;
		var referenceElement = this.state.referenceElement;

		// Get layers
		var layers = self.state.data.getIn([ 'layers', 'ids' ])
			.map(function(layerId) { return self.state.data.getIn([ 'layers', 'hash', layerId ]); })
			.filter(function(obj) { return obj.get('referenceElement') === referenceElement; });

		// Return
		return (
			<div className='contextual-layer-group' ref={function(c) { self._layerGroupRef = c; }}>
				{layers.map(function(layerItem, layerIndex) {

					// Data
					var id = layerItem.get('id');
					var position = layerItem.get('position');
					var helper = layerItem.get('helper');
					var children = layerItem.get('children');
					var referenceElement = layerItem.get('referenceElement');
					var isHidden = layerItem.get('isHidden');
					var fitToHelper = layerItem.get('fitToHelper');

					// Get layer data
					var positionerClasses = classNames('contextual-layer-positioner', { 'hidden-element': isHidden });
					var width = fitToHelper ? helper.getBoundingClientRect().width : null;
					var height = fitToHelper ? helper.getBoundingClientRect().height : null;
					var offsetLeft = helper.getBoundingClientRect().left - referenceElement.getBoundingClientRect().left;
					var offsetCenter = (helper.getBoundingClientRect().left + (helper.getBoundingClientRect().width * 0.5)) - referenceElement.getBoundingClientRect().left;
					var offsetRight = helper.getBoundingClientRect().right - referenceElement.getBoundingClientRect().left;
					var offsetTop = helper.getBoundingClientRect().top - referenceElement.getBoundingClientRect().top;
					var offsetMiddle = (helper.getBoundingClientRect().top + (helper.getBoundingClientRect().height * 0.5)) - referenceElement.getBoundingClientRect().top;
					var offsetBottom = helper.getBoundingClientRect().bottom - referenceElement.getBoundingClientRect().top;

					// Set positioner to belowLeft
					if(position === 'belowLeft' || position === null) {
						return (
							<div className={positionerClasses} style={{ top: offsetBottom, left: offsetLeft }} key={layerIndex}>
								<div className='contextual-layer belowleft' style={{ width: width }}>{children}</div>
							</div>
						);
					}

					// Set positioner to belowRight
					else if(position === 'belowRight') {
						return (
							<div className={positionerClasses} style={{ top: offsetBottom, left: offsetRight }} key={layerIndex}>
								<div className='contextual-layer belowright' style={{ width: width }}>{children}</div>
							</div>
						);
					}

					// Set positioner to aboveCenter
					else if(position === 'belowCenter') {
						return (
							<div className={positionerClasses} style={{ top: offsetBottom, left: offsetCenter }} key={layerIndex}>
								<div className='contextual-layer belowcenter'>
									<div className='contextual-layer-children' style={{ width: width }}>{children}</div>
								</div>
							</div>
						);
					}


					// Set positioner to aboveLeft
					else if(position === 'aboveLeft') {
						return (
							<div className={positionerClasses} style={{ top: offsetTop, left: offsetLeft }} key={layerIndex}>
								<div className='contextual-layer aboveleft' style={{ width: width }}>{children}</div>
							</div>
						);
					}

					// Set positioner to aboveRight
					else if(position === 'aboveRight') {
						return (
							<div className={positionerClasses} style={{ top: offsetTop, left: offsetRight }} key={layerIndex}>
								<div className='contextual-layer aboveright' style={{ width: width }}>{children}</div>
							</div>
						);
					}

					// Set positioner to aboveCenter
					else if(position === 'aboveCenter') {
						return (
							<div className={positionerClasses} style={{ top: offsetTop, left: offsetCenter }} key={layerIndex}>
								<div className='contextual-layer abovecenter'>
									<div className='contextual-layer-children' style={{ width: width }}>{children}</div>
								</div>
							</div>
						);
					}


					// Set positioner to beforeTop
					else if(position === 'beforeTop') {
						return (
							<div className={positionerClasses} style={{ top: offsetTop, left: offsetLeft }} key={layerIndex}>
								<div className='contextual-layer beforetop' style={{ height: height }}>{children}</div>
							</div>
						);
					}

					// Set positioner to beforeBottom
					else if(position === 'beforeBottom') {
						return (
							<div className={positionerClasses} style={{ top: offsetBottom, left: offsetLeft }} key={layerIndex}>
								<div className='contextual-layer beforebottom' style={{ height: height }}>{children}</div>
							</div>
						);
					}

					// Set positioner to beforeMiddle
					else if(position === 'beforeMiddle') {
						return (
							<div className={positionerClasses} style={{ top: offsetMiddle, left: offsetLeft }} key={layerIndex}>
								<div className='contextual-layer beforemiddle'>
									<div className='contextual-layer-children' style={{ height: height }}>{children}</div>
								</div>
							</div>
						);
					}


					// Set positioner to afterTop
					else if(position === 'afterTop') {
						return (
							<div className={positionerClasses} style={{ top: offsetTop, left: offsetRight }} key={layerIndex}>
								<div className='contextual-layer aftertop' style={{ height: height }}>{children}</div>
							</div>
						);
					}

					// Set positioner to afterBottom
					else if(position === 'afterBottom') {
						return (
							<div className={positionerClasses} style={{ top: offsetBottom, left: offsetRight }} key={layerIndex}>
								<div className='contextual-layer afterbottom' style={{ height: height }}>{children}</div>
							</div>
						);
					}

					// Set positioner to afterMiddle
					else if(position === 'afterMiddle') {
						return (
							<div className={positionerClasses} style={{ top: offsetMiddle, left: offsetRight }} key={layerIndex}>
								<div className='contextual-layer aftermiddle'>
									<div className='contextual-layer-children' style={{ height: height }}>{children}</div>
								</div>
							</div>
						);
					}

				})}
			</div>
		);
		
	},

	// On change event
	_onChange: function() {
		if(this._isMounted) this.setState({ data: getState() });
	},

	// On body resize event
	_onWindowResize: function() {
		setTimeout(ContextualLayerGroupActions.updateLayers, 1);
	},

	// On parent resize event
	_onParentResize: function() {

		// Return if group is no longer instanciate
		if(this._layerGroupRef === null) {
			return;
		}

		// Data
		var parentWidth = this._layerGroupRef.parentElement.getBoundingClientRect().width;

		// Check if parent width is not defined
		if(typeof this._parentWidth === 'undefined') {
			this._parentWidth = parentWidth;
			return;
		}

		// If parent width has changed
		if(this._parentWidth !== parentWidth) {

			// Update layers
			setTimeout(ContextualLayerGroupActions.updateLayers, 1);

		}

		// Update parent width
		this._parentWidth = parentWidth;

	}

});


// Get reference element
 var _getReferenceElement = function(layerGroup) {

 	// Data
 	var i;
 	var parentElement;
 	var parentElementClassNames;
 	var layerGroupParents = $(layerGroup).parents();
 	var referenceElement = null;

	// Loop over layer group parents
	for(i = 0; i < layerGroupParents.length; i++) {

		// Data
		parentElement = layerGroupParents[i];
 		parentElementClassNames = parentElement.className.split(' ');

		// If scroll
		if(_.contains(parentElementClassNames, 'scroll-y') || _.contains(parentElementClassNames, 'scroll-x')) {
			referenceElement = parentElement.children[0];
			break;
		}

		// If window
		if(_.contains(parentElementClassNames, 'contextual-window')) {
			referenceElement = parentElement.children[0];
			break;
		}

		// If content
		else if(parentElement.id === 'content') {
			referenceElement = parentElement.children[0];
			break;
		}

		// If body
		else if(parentElement.tagName.toLowerCase() === 'body') {
			referenceElement = parentElement.children[0].children[0];
			break;
		}

	}

	// Return reference element
	return referenceElement;

};


// Exports component
module.exports = ContextualLayerGroup;