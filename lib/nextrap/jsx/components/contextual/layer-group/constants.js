var keyMirror = require('keymirror');


// Exports constants
module.exports = keyMirror({

  	CONTEXTUAL_LAYER_GROUP_MOUNT_LAYER: null,
  	CONTEXTUAL_LAYER_GROUP_UPDATE_LAYER: null,
  	CONTEXTUAL_LAYER_GROUP_UPDATE_LAYERS: null,
  	CONTEXTUAL_LAYER_GROUP_UNMOUNT_LAYER: null,
  	CONTEXTUAL_LAYER_GROUP_TOGGLE_LAYER: null
  	
});