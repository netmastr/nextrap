// Data
var config = {};

// Set default INTL_MESSAGES
config.INTL_MESSAGES = {
	'fr-FR': require('../intl/fr-FR_messages.js'),
	'en-EN': require('../intl/en-EN_messages.js')
};

// Set default locale
config.locale = 'fr-FR';

// Set locale messages
config.localeMessages = config.INTL_MESSAGES[config.locale];


// Exports
module.exports = config;