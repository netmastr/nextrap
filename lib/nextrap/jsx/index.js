// Get config
var config = require('./config');

// Exports
module.exports = {

	// Set dispatcher
	Dispatcher: require('../dispatcher'),

	// Set contextual
	ContextualLayerGroup: require('./components/contextual/layer-group/component.react'),
	ContextualLayer: require('./components/contextual/layer.react'),
	Dropdown: require('./components/contextual/dropdown.react'),
	Tooltip: require('./components/contextual/tooltip.react'),
	ContextualWindowGroup: require('./components/contextual/window-group.react'),
	ContextualWindow: require('./components/contextual/window.react'),
	Modal: require('./components/contextual/modal.react'),

	// Set elements
	ButtonGroup: require('./components/elements/button-group.react'),
	ButtonList: require('./components/elements/button-list.react'),
	ButtonListItem: require('./components/elements/button-list-item.react'),
	Button: require('./components/elements/button.react'),
	Avatar: require('./components/elements/avatar.react'),
	Address: require('./components/elements/address.react'),
	Email: require('./components/elements/email.react'),
	Phone: require('./components/elements/phone.react'),
	Job: require('./components/elements/job.react'),
	Link: require('./components/elements/link.react'),
	HNavList: require('./components/elements/h-nav-list.react'),
	HNavItem: require('./components/elements/h-nav-item.react'),
	VNavList: require('./components/elements/v-nav-list.react'),
	VNavItem: require('./components/elements/v-nav-item.react'),
	Table: require('./components/elements/table.react'),

	// Set form
	FormGroup: require('./components/form/form-group.react'),
	ControlAutocomplete: require('./components/form/control-autocomplete.react'),
	ControlAutocompleteTag: require('./components/form/control-autocomplete-tag.react'),
	ControlCheckbox: require('./components/form/control-checkbox.react'),
	ControlDate: require('./components/form/control-date.react'),
	ControlFile: require('./components/form/control-file.react'),
	ControlGroup: require('./components/form/control-group.react'),
	ControlLabel: require('./components/form/control-label.react'),
	ControlPhoto: require('./components/form/control-photo.react'),
	ControlRadio: require('./components/form/control-radio.react'),
	ControlSelect: require('./components/form/control-select.react'),
	ControlSelectButtons: require('./components/form/control-select-buttons.react'),
	ControlShare: require('./components/form/control-share.react'),
	ControlStatic: require('./components/form/control-static.react'),
	ControlText: require('./components/form/control-text.react'),

	// Update locale function
	updateLocale: function(locale, messages) {
		config.INTL_MESSAGES[locale] = messages;
	},

	// Change locale function
	locale: function(locale) {

		// Check if locale is defined
		if(typeof config.INTL_MESSAGES[locale] === 'undefined') {
			throw new Error('The provided locale is not defined');
		}

		// Set config
		config.locale = locale;
		config.localeMessages = config.INTL_MESSAGES[config.locale];
		
	}

};