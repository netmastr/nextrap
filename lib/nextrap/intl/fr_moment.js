module.exports = function(Moment) {
	Moment.defineLocale('fr', {
        months: 'Janvier_Février_Mars_Avril_Mai_Juin_Juillet_Août_Septembre_Octobre_Novembre_Décembre'.split('_'),
        monthsShort: 'Janv._Févr._Mars_Avr._Mai_Juin_Juil._Août_Sept._Oct._Nov._Déc.'.split('_'),
        weekdays: 'Dimanche_Lundi_Mardi_Mercredi_Jeudi_Vendredi_Samedi'.split('_'),
        weekdaysShort: 'Dim._Lun._Mar._Mer._Jeu._Ven._Sam.'.split('_'),
        weekdaysMin: 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        longDateFormat: {
            LT: 'HH:mm',
            LTS: 'HH:mm:ss',
            L: 'DD/MM/YYYY',
            LL: 'D MMMM YYYY',
            LLL: 'D MMMM YYYY HH:mm',
            LLLL: 'dddd D MMMM YYYY HH:mm'
        },
        calendar: {
            sameDay: function() {
                return '[' + this.fromNow(true) + ']';
            },
            nextDay: '[Demain à] LT',
            nextWeek: 'dddd [à] LT',
            lastDay: '[Hier à] LT',
            lastWeek: 'dddd [à] LT',
            sameElse: function() {
                if(this.isSame(Date.now(), 'year')) {
                    return 'D MMMM [à] LT';
                } else {
                    return 'L [à] LT';
                }
            }
        },
        relativeTime: {
            future: 'Dans %s',
            past: 'Il y a %s',
            s: function (number, withoutSuffix, key, isFuture) {
                if(number < 5 && withoutSuffix) {
                    return 'Maintenant';
                } else if(number < 5 && !withoutSuffix) {
                    return 'quelques secondes';
                } else {
                    return number + ' s';
                }
            },
            m: '%d min',
            mm: '%d min',
            h: '%d h',
            hh: '%d h',
            d: '%d jour',
            dd: '%d jours',
            M: '%d mois',
            MM: '%d mois',
            y: '%d an',
            yy: '%d ans'
        },
        ordinalParse: /\d{1,2}(er|)/,
        ordinal: function (number) {
            return number + (number === 1 ? 'er' : '');
        },
        week: {
            dow: 1, // Monday is the first day of the week.
            doy: 4  // The week that contains Jan 4th is the first week of the year.
        }
    });
};