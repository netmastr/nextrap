module.exports = {
	entity: {
		type: '{type, select,' +
			'customer {Client}' +
			'provider {Fournisseur}' +
	        'other {}' +
		'}',
	    phoneType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'cell {Mobile}' +
			'second {Secondaire}' +
	        'other {}' +
		'}',
	    emailType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'second {Secondaire}' +
	        'other {}' +
		'}',
		addressType: '{type, select,' +
			'home {Maison}' +
			'workplace {Bureau}' +
			'billing {Facturation}' +
			'headquarter {Siège social}' +
	       ' other {}' +
		'}',
	},
	keywords: {
		at: 'Chez',
		workAt: 'Travaille chez',
	}
};