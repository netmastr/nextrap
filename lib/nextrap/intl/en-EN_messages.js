module.exports = {
	entity: {
		type: '{type, select,' +
			'customer {Customer}' +
			'provider {Provider}' +
	        'other {}' +
		'}',
	    phoneType: '{type, select,' +
			'home {Home}' +
			'workplace {Office}' +
			'cell {Mobile}' +
			'second {Secondary}' +
	        'other {}' +
		'}',
	    emailType: '{type, select,' +
			'home {Home}' +
			'workplace {Office}' +
			'second {Secondary}' +
	        'other {}' +
		'}',
		addressType: '{type, select,' +
			'home {Home}' +
			'workplace {Office}' +
			'billing {Billing}' +
			'headquarter {Headquarter}' +
	       ' other {}' +
		'}',
	},
	keywords: {
		at: 'At',
		workAt: 'Work at',
	}
};